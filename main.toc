\contentsline {chapter}{Chapter 1: Cosmology}{1}{chapter*.5}%
\contentsline {section}{\numberline {1.1}A brief history of cosmology}{2}{section.7}%
\contentsline {section}{\numberline {1.2}The expanding universe}{3}{section.9}%
\contentsline {section}{\numberline {1.3}Inflation}{9}{section.11}%
\contentsline {section}{\numberline {1.4}The CMB}{15}{section.15}%
\contentsline {section}{\numberline {1.5}Challenges in measuring the CMB}{24}{section.24}%
\contentsline {section}{\numberline {1.6}Thesis outline}{25}{section.26}%
\contentsline {chapter}{Chapter 2: SPIDER}{27}{chapter*.28}%
\contentsline {section}{\numberline {2.1}Ballooning from Antarctica}{28}{section.31}%
\contentsline {section}{\numberline {2.2}Payload components}{30}{section.33}%
\contentsline {section}{\numberline {2.3}Inserts and optics}{32}{section.37}%
\contentsline {section}{\numberline {2.4}Detectors and readout}{35}{section.42}%
\contentsline {section}{\numberline {2.5}STACY and WENDY}{43}{section.52}%
\contentsline {chapter}{Chapter 3: TESs and MCEs}{46}{chapter*.55}%
\contentsline {section}{\numberline {3.1}Physical descriptions of TESs}{47}{section.57}%
\contentsline {section}{\numberline {3.2}Thermal and electrical model for the TES}{48}{section.59}%
\contentsline {section}{\numberline {3.3}TES behavior}{54}{section.64}%
\contentsline {section}{\numberline {3.4}TES Simulations}{59}{section.70}%
\contentsline {chapter}{Chapter 4: SPIDER Noise}{61}{chapter*.73}%
\contentsline {section}{\numberline {4.1}Expected Noise}{62}{section.76}%
\contentsline {section}{\numberline {4.2}Data selection}{65}{section.78}%
\contentsline {section}{\numberline {4.3}Fitting detectors}{68}{section.83}%
\contentsline {section}{\numberline {4.4}Fitting readout and excess noise}{71}{section.85}%
\contentsline {section}{\numberline {4.5}Discussion}{73}{section.89}%
\contentsline {chapter}{Chapter 5: \textsc {CMB-S4}\xspace Detector Design}{77}{chapter*.91}%
\contentsline {section}{\numberline {5.1}\textsc {CMB-S4}\xspace design}{79}{section.95}%
\contentsline {section}{\numberline {5.2}Parameter search}{81}{section.102}%
\contentsline {section}{\numberline {5.3}Further work}{82}{section.106}%
\contentsline {chapter}{Chapter 6: The Monotile}{85}{chapter*.108}%
\contentsline {section}{\numberline {6.1}{\it {Planck}}\xspace 's experience}{86}{section.110}%
\contentsline {section}{\numberline {6.2}Experimental data and analysis}{88}{section.113}%
\contentsline {section}{\numberline {6.3}Analysis}{93}{section.119}%
\contentsline {section}{\numberline {6.4}Results}{96}{section.120}%
\contentsline {section}{\numberline {6.5}Steps}{100}{section.127}%
\contentsline {section}{\numberline {6.6}Discussion}{101}{section.129}%
\contentsline {chapter}{Appendix A: More Cosmology Equations}{104}{appendix*.132}%
\contentsline {section}{\numberline {A.1}General cosmology derivations}{104}{section.133}%
\contentsline {section}{\numberline {A.2}Inflation derivations}{107}{section.134}%
\contentsline {section}{\numberline {A.3}$E$ and $B$ decomposition}{120}{section.137}%
\contentsline {chapter}{Appendix B: Code}{121}{appendix*.139}%
\contentsline {section}{\numberline {B.1}MCE speedup scripts}{121}{section.140}%
\contentsline {section}{\numberline {B.2}EST}{123}{section.141}%
\contentsline {section}{\numberline {B.3}GEANT4 sims}{126}{section.142}%
\contentsline {chapter}{Appendix C: Extra Results}{131}{appendix*.144}%
\contentsline {section}{\numberline {C.1}Leg analysis}{131}{section.145}%
\contentsline {subsection}{\numberline {C.1.1}Building the catalogue}{131}{subsection.146}%
\contentsline {subsection}{\numberline {C.1.2}Weight}{133}{subsection.148}%
\contentsline {section}{\numberline {C.2}Thermal model}{133}{section.149}%
\contentsline {section}{\numberline {C.3}\textsc {CMB-S4}\xspace ($L,C$) choices}{135}{section.151}%
\contentsline {chapter}{Bibliography}{137}{section.151}%
