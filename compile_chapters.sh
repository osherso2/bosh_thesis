cd chapters
for chp in ./*;
do
    cd $chp
    pwd
    pdflatex main_chapter.tex > log_compile
    bibtex main_chapter >> log_compile
    cd ..
done
cd ..

cd appendices
for appn in ./*;
do
    cd $appn
    pwd
    pdflatex main_chapter.tex >> log_compile
    bibtex main_chapter >> log_compile
    cd ..
done
cd ..

echo "global compile"
pdflatex main.tex >> log_compile
echo "global bibtex"
bibtex main >> log_compile
echo "global compile"
pdflatex main.tex >> log_compile
echo "making copy"
cp main.pdf bosh_thesis.pdf 
