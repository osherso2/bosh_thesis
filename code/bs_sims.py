from revX150 import *
import matplotlib.pyplot as plt
import numpy as np

det.halfway_finder()
state = det.save_pkl()
reset = lambda: det.load_pkl(state=state)

t_arr = np.arange(0, .5, 3e-8)
l = len(t_arr)
Vb = np.zeros(l)*det.Vb + .005
Vb[int(l/4):] -=  .005
Vb[int(2*l/4):] +=  .005
Vb[int(3*l/4):] -=  .005
Vb = np.roll(Vb, l/8)

rI = det.I
rT = det.T

sc = False 
I_arr = np.array([])
b_arr = np.arange(.06, .16, .015)[::-1]
c = lambda i: plt.cm.inferno((i + .5)/(1.+len(b_arr)))
det.find_equil(b_arr[0])
for i, b in enumerate(b_arr):
    det.Vb = b
    det.settle()
    det.settle()
    f = det.R/det.TESs[0].Rn
    if f < .1: 
        if sc:
            print i, b, det.Vb, "fail"
            continue
        sc = True
    print i, b, det.Vb, f 
    t, _, _, I, _ = det.Vb_variation(t_arr, Vb+b)
    I = (I - I[-1])*1e6
    I_arr = np.append(I_arr, I)
    plt.plot(t, I, lw=2, color=c(i), label=r"$R/R_n = $%f" % f, alpha=.65)
plt.legend()
plt.xlim((0, max(t)))
plt.xlabel("time [s]")
plt.ylabel(r"$\Delta I [\mu A]$")
plt.show()
