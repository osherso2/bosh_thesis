import numpy, matplotlib.pyplot as plt
from scipy.special import hankel1 as hank

eta = -1.
k = numpy.logspace(-3, 3, 10000)
dpi = 2.*numpy.pi

def spec(k, eta):
    
    z = k*abs(eta)
    c = z**(3./2.) / (4.*dpi)**.5
    return c*numpy.abs(hank(3./2., z))

plt.plot(dpi/(abs(eta)*k), spec(k, eta))
plt.xscale("log")
plt.yscale("log")
plt.xlabel(r"$2\pi/(k|\eta|)$")
plt.ylabel(r"$\Delta_{\delta\phi}/H$")
plt.show()
