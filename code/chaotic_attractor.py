import numpy, matplotlib, matplotlib.pyplot as plt
matplotlib.rc('font', size=22)

# Choice in time step and setting the gravitational constant to unity.
dt = .002; k = 1.

# Friedmann equation governing H evolution
# This is actually H divided by m!
def Friedmann(phi, dphi, k=1.):

    H = (k*(dphi**2. + phi**2.)/6.)**.5
    return H

# Differential equation governing phi and H
def evolve(phi, dphi, dt=.01, k=1.):

    H = Friedmann(phi, dphi, k=k)
    ddphi = -3.*H*dphi - phi
    phi += dphi*dt
    dphi += ddphi*dt

    return phi, dphi, H


y = 2.5 # initial dphi value
x = 8.0 # range limit on initial phi value

# attractor dphi value
c = (2./3.)**.5

# initial field
phi_i = 15.
dphi_i = -c
H_i = Friedmann(phi_i, dphi_i, k=k)

time = numpy.arange(50000)*dt
phi = [phi_i]; dphi = [dphi_i]
scale_factor = [1.]; H = [H_i]

# Evolving the universe
for t in time:
    phi_i, dphi_i, h = evolve(phi_i, dphi_i, dt=dt, k=k)
    phi.append(phi_i)
    dphi.append(dphi_i)
    H.append(h)

phi = numpy.array(phi)
dphi = numpy.array(dphi)
attractor_phi = phi.copy()
attractor_dphi = dphi.copy()
attractor_H = numpy.array(H)

plot_attractor = True
if plot_attractor:
    plt.axhline(0, color='k')
    plt.axvline(0, color='k')
    plt.axhline(c, ls=":", color='k')
    plt.axhline(-c, ls=":", color='k')

    plt.plot(phi, dphi, lw=7, color='k', alpha=.5, label="attractor solution")
    plt.plot(-phi, -dphi, lw=7, color='k', alpha=.5)

    for a in numpy.arange(-x-1, x+2):
        for b in [-y, y]:
            phi_i = a; dphi_i = b
            phi = [phi_i]; dphi = [dphi_i]
            for t in time:
                phi_i, dphi_i, _ = evolve(phi_i, dphi_i, dt=dt, k=k)
                phi.append(phi_i)
                dphi.append(dphi_i)
            plt.plot(phi, dphi)

    plt.legend()
    plt.tight_layout()
    plt.xlim((-x, x))
    plt.ylim((-y, y))
    plt.xlabel(r"$\phi$")
    plt.gca().xaxis.set_label_coords(.95, 0)
    ylbl = plt.ylabel(r"$\partial_\tau \phi$")
    ylbl.set_rotation(0)
    ylbl.set_horizontalalignment('left')
    plt.gca().yaxis.set_label_coords(0.005, .963)
    plt.xticks([-6, -4, -2, 0, 2, 4, 6])
    plt.yticks([-c, 0., c], [r"$-\sqrt{\frac{2}{3k_G}}$","0",r"$\sqrt{\frac{2}{3k_G}}$"])
    plt.show()


plot_scaling = False
if plot_scaling:
    def gradient_shading(xmn, xmx, color='r', label=None, ymn=-1, ymx=11):

        for i in numpy.linspace(0, 2, 25):
            plt.axvspan(xmn+i, xmx-i, ymin=ymn, ymax=ymx, color=color, alpha=.01)
        plt.axvspan(-10, -5, ymin=-100, ymax=-90, color=color, alpha=.25, label=label)

    plt.subplot(311)
    gradient_shading(-20, 17, color='g', label="slow roll")
    gradient_shading(15, 20, color='b', label="graceful exit")
    gradient_shading(18, 40, color='r', label="ineffective model")

    plt.axhline(0, color='k')
    attractor_phi = attractor_phi[:-1]
    plt.plot(time, attractor_phi)

    plt.xlim((0, 25))
    plt.ylim((-1, 15))
    plt.yticks([0, 5, 10, 15])
    plt.xticks([])
    ylbl = plt.ylabel(r"$\phi$")
    ylbl.set_rotation(0)
    ylbl.set_horizontalalignment('left')
    plt.gca().yaxis.set_label_coords(0.005, .875)
    plt.legend(loc="upper right", fontsize=20)

    plt.subplot(312)
    gradient_shading(-20, 17, color='g', label="slow roll")
    gradient_shading(15, 20, color='b', label="graceful exit")
    gradient_shading(18, 40, color='r', label="ineffective model")

    plt.axhline(0, color='k')
    H = H[:-1]
    plt.plot(time, H)

    plt.xlim((0, 25))
    plt.ylim(ymin=0)
    plt.yticks([0, 2, 4, 6])
    plt.xticks([])
    ylbl = plt.ylabel(r"$H/m$")
    ylbl.set_rotation(0)
    ylbl.set_horizontalalignment('left')
    plt.gca().yaxis.set_label_coords(0.005, .875)

    plt.subplot(313)
    gradient_shading(-20, 17, color='g', label="slow roll")
    gradient_shading(15, 20, color='b', label="graceful exit")
    gradient_shading(18, 40, color='r', label="ineffective model")

    # This is the dtau = m dt integral of my rescaled H (so really H/m),
    # so this is int (H/m) m dt = ln(a)! Actually returns efolds.
    efolds = numpy.cumsum(H)*dt
    plt.axhline(0, color='k')
    plt.plot(time, efolds)

    plt.xlim((0, 25))
    plt.ylim(ymin=0)
    #ylbl = plt.ylabel(r"$e$-folds$\cdot k_G$")
    ylbl = plt.ylabel(r"$e$-folds")
    ylbl.set_rotation(0)
    ylbl.set_horizontalalignment('left')
    plt.gca().yaxis.set_label_coords(0.005, .875)
    plt.xlabel(r"$\tau = t \cdot m$")
    plt.xticks(numpy.arange(0,25,5))
    plt.yticks(numpy.arange(0,61,10))

    plt.show()
