from revX150 import *
import matplotlib.pyplot as plt

det.halfway_finder()

c = lambda i: plt.cm.inferno(float(i)/5.)
for i, e in enumerate([100, 1000, 10000, 100000,
 1000000]):
    t, _, I, T = det.cray(E=e)
    I -= I[0]; I *= 1e6
    dT = 1e3*(max(T) - min(T))
    t -= .02
    plt.plot(t, I, c=c(i), lw=2.5, label=r"$\Del
ta Q = $%.0e$ eV \rightarrow \Delta T = $%.1e mK" % (e, dT))
plt.xlabel("time [s]")
plt.ylabel(r"$\Delta I$ [$\mu$ A]")
plt.ylim((min(I) - .1, .1))
plt.xlim((-0.01, 0.1))
#plt.yscale("symlog", linthreshy=1e-3)
#plt.yscale("log")
#plt.legend()
plt.show()

