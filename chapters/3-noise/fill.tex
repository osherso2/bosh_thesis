\chapter*{Chapter 4\\SPIDER Noise}
\addcontentsline{toc}{chapter}{Chapter 4: SPIDER Noise}
\def \chapterlabel {4}
\label{nse}
\setcounter{eqnum}{1}\setcounter{fignum}{1}

\begin{introfig}{crashed_spider.jpg}{nse:intro_fig}
%
\spiderI crashed in the antarctic ice.
%
Photo credit: Sam Burrell of the BAS.
%
\end{introfig}
\clearpage



\spiderI noise is complicated and warrants investigation.
%
To examine this, I model the detectors using a variety of sources of information to constrain the  detector parameters necessary to model the moise.
%
With this noise model I can make comparisons against data taken specifically for noise analysis.



\spiderI was noisier than anticipated, which we believe to be largely due to RFI.
%
The most obvious cases of RFI are large features in the data correspond to emissions from the telecommunication instruments in the SIP.
%
Other kinds of noise include reaction wheel noise (RWN) which has been well correlated to the changing position of the reaction wheels arms.
%
The mechanism is likely related to moving arms changing scatters and reflections of RF about the cryostat.
%
Another identified noise phenomenon, termed \newterm{popcorn noise} because of the way it burst on and off, was reproduced in the lab by pinching the cooling fins of the MCE which could also be related to RFI or possibly grounding.
%
While glitches in data caused by telecommunications are easy to identify because their high signal to noise, and RWN noise is well removed using \spiderI's log of reaction wheel angles, the more subtle noise phenomena like residual popcorn noise that eluded flagging can elevated map noise.



To aid in better understanding residual noises, this analysis disentangles noise from the TESs themselves and noise originating from the readout chain.
%
While it is disappointing that \spiderI's shielding%
\footnote{Or possibly ground strategy.} %
was not better, this analysis identifies gaps in our current understanding of these detectors and supplies a noise model that can inform future experiment proposals.



The noise model is described in detail in the next section.
%
After the noise model descriptions I describe the data to compare against the models in \ref{nse:data_sets}.
%
The detector model fit, which fills in our library of detector parameters and is used in the noise model fit, is described in \ref{nse:fit_det_models}.
%
I then describe the noise model fit in detail in \ref{nse:fit_nse_models}.
%
I end this chapter with some discussion about additional directions to investigate and possible improvements to the data taken in flight.



\section{Expected Noise}\label{nse:comp_models}



In this section I describe the expected noise components in a \spider detectors and the readout system.
%
We treat all of these components as gaussian processes, so our noise spectra describe the expected variances per frequency bin.
%
For another treatment on everything to do with TESs, see \cite{chapter:Irwin_Hilton:TES:2005}.
%
This treatment follows Alexandra ``Sasha'' Rahlin's notation from her thesis, \cite{thesis:Rahlin:2016}.



\subsection*{Detector Noise}



The previous chapter (\ref{dets}) covers the expected behavior of TES detectors.
%
Many of the same parameters are used here, with a few additions, to describe the expected noise from such a detector. 



Each of the following noise components is best expressed as a variation in power on the TES.
%
These variations per bin in time (or frequency) are distributed normally, so can quantify these variations by their standard deviations, $\delta P$, which can then be treated as inputs to the matrix in \myref{dets:tes_linear}.
%
In this treatment, the vector $\svec{\delta Q} = (I_0 \delta V, \delta Q) = (\delta P_J, \delta P_Q)$ where $\delta P_J$ is the variation in the Joule power and $\delta P_Q$ is the variation in deposited power:
%
\begin{itemize}
%
\item
%
Photons hit the detector at random times with varying energies which adds jitter to the power flowing from the antennas to the detector islands:
%
\begin{namebox}{Photon noise}{nse:photon_noise}
%
\begin{gather*}
%
\delta P_{Q}^{\gamma} = \sqrt{2 h \nu_c P_{opt} + 2 \frac{P_{opt}^2}{\nu_{BW}}}
%
\end{gather*}
%
Where $\nu_c$ is the central observing frequency, $\nu_{BW}$ is the width of the band.
%
The first term, called the as \newterm{shot noise}, is dominant in the case where $P_{opt}$ is small, while the latter term adds in the bunching effect of photons which is dominant at higher incoming photon rates or narrow bandwidths.
%
See \cite{Hubmayr:2018} for more detail on similar detectors.
%
\end{namebox}
%
\item
%
Phonons scatter onto and off of the detector island from and to the bulk of the silicon along the legs of the island.
%
This adds thermodynamic noise:
%
\begin{namebox}{Phonon noise}{nse:phonon_noise}
%
\begin{gather*}
%
\delta P_{Q}^{phonon} = \sqrt{4 k_B T^2 G F_p} 
%
\end{gather*}
%
Where $F_p$ ($\sim 0.5$ for \spiderI) is an adjustment factor which accounts for the configuration of the legs like the geometry and the temperature gradient between the island and the wafer.
%
\end{namebox}
%
\item
%
Noise from the thermal agitation in conductors \cite{Johnson:1928} can be treated as an added in-series voltage source.
%
Both the shunt resistor and the TES itself have these thermal agitations.
%
In the shunt resistor, which is alway from the TES island, this adds jitter to the current through the TES but not to power balance directly.
%
In the TES case this voltage fluctuation dissipates power on the island itself.
%
Because an on-transition TES maintains its saturation power, $\delta P_Q = -\delta P_J$, we have: 
%
\begin{namebox}{Johnson noise}{nse:johnson_noise}
%
\begin{gather*}
%
\delta P_{J}^{R_{sh}} = I_0 \sqrt{4 k_B T_b R_{sh}}
%
\end{gather*}
%
For TESs an extra $2\beta_I$ term accounts for the non-linearity of a TES. 
\begin{align*}
%
\delta P_{Q}^{TES} &=-I_0 \sqrt{4 k_B T R_0 (1 + 2\beta_I)}\\
\delta P_{J}^{TES} &= I_0 \sqrt{4 k_B T R_0 (1 + 2\beta_I)}
%
\end{align*}
%
\end{namebox}
%
\end{itemize}



We want to compare expected noise to measured noise, which is measured as TES current by the MCE.
%
To convert from $\svec{\delta Q}$ to \newterm{noise equivalent current} (NEI), we use the matrix:



\begin{namebox}{NEP to NEI}{nse:NEI_conv}
%
Continuing from \myref{dets:tes_linear} where these expressions are defined:
%
\begin{gather*}
%
\delta I(\omega) = \frac{1}{W} \left( \frac{L}{I_0} \left(i \omega + \tau_{ele}^{-1}\right) \sps \frac{G\mathcal{L}_I}{LCI_0}  \right) \cdot \left( \begin{array}{cc} \delta P_J \\ \delta P_Q  \end{array} \right) 
%
\end{gather*}
%
Where $\delta I(\omega)$ is the NEI and $\delta P_X^2 = \sum \left(\delta P_X^{source}\right)^2$ for both $X = J, Q$.
%
\end{namebox}


A notable effect of this transformation is that the sources of noise from the TES are filtered by the TES time constants, $\tau_\pm$.
%
The heat fluctuations from phonon and photon noise can not make current fluctuations any faster than $\tau_-$, the TES's Johnson noise is constrained between $\tau_+$ and $\tau_-$, and the shunt Johnson noise can add noise like TES Johnson noise plus induce thermal fluctuations similar to phonon noise, giving it a stepped appearance.
%
These shapes are shown in \myref{nse:total_sim}.



\subsection*{$m$-noise}



This combined model underestimates noise measured in real TESs across many experiments as a high amount of \newterm{excess noise} has always been found in TESs.
%
There are a few varied models to explain this excess \cite{Fraser:2004, Lindeman:2006, Wessels:2019}. 



Despite the mysterious origins of these noise sources, the excess NEI is similar to the TES Johnshon NEI, and is cut-off by the same detector time constants.
%
To model excess noise we create one new parameter, $m$, and model the noise from the TES as $\sqrt{m^2 + 1} \cdot \delta I_{TES}$:



\begin{namebox}{$m$-noise}{nse:excess}
%
\begin{gather*}
%
\delta I_{m} = m \cdot \delta I_{TES}
%
\end{gather*}
%
Measurements on older \spiderI-style devices that never made flight found $m$ valuess of $6-7$.
%
\end{namebox}



To avoid confusion with other kinds of noise in excess of expectations, I call this noise component the \newterm{$m$-noise}.



\subsection*{Total noise}



The final source of noise we add is from the readout.
%
However, the readout noise is natively in NEI, so it is added after the conversion from \myref{nse:NEI_conv} is applied.
%
The main part of this noise is the SQUID plateau, which is just white noise cut off at the SQUID bandwidth:



\begin{namebox}{SQUID noise}{nse:sq_plat}
%
\begin{gather*}
%
\delta I_{SQ}(\omega) = \frac{\iota_{SQ}}{\sqrt{1 + \left( \frac{\omega}{2\pi f^{SQ}_{BW}} \right)}}
%
\end{gather*}
%
Where $\iota_{SQ}$ is the (nominally $2.5 pA/\sqrt{Hz}$) plateau and $f^{SQ}_{BW} = 2MHz$.
%
\end{namebox}



Dark SQUID (DS) channels should act as sentinels for readout noise as they have no connected TESs.
%
By design, the noise on these DSs should be similar to the readout noise for TES equipped channels.



Combining all of these noise sources together yields our expected total detector noise, but we must also account for aliasing.



\begin{boxfig}{Total noise simulation}{noise_sim.png}{nse:total_sim}
%
All noise components added together and aliased at $15kHz$ for a simulated $150 GHz$ detector. 
%
Because of its large bandwidth, the readout noise which was subdominant pre-aliasing at low frequencies becomes one of the larger contributors to noise, highlighting the importance of bandwidth and aliasing considerations.
%
The frequencies relevant to the CMB analysis, $1-3 Hz$, are highlighted in blue.
%
Notice that in these frequencies the noise is rather flat since the $\tau_-$ rising edge is not significant until $\sim 10 Hz$.
%
\end{boxfig}



One consequence of aliasing is the loss of features.
%
For example, a peak in the intrinsic spectrum at high frequency with a quality factor of $\sim 100$, (\eg in the full width at half maximum defined interval $100 kHz \pm 500 Hz$), contributes a nearly flat increase in the data's bandwidth after aliasing.
%
This means that without high sample-rate data, RF absorbed by SQUIDs is indistinguishable from excessively high readout plateaus even if they had sharp resonances.



\section{Data selection}\label{nse:data_sets}



To test the above noise component models, I consider two in-flight data sets.
%
The first set consisted of periods where \spiderI executed a \newterm{stare}, during which the pointing was fixed so the cosmic signal did not vary.
%
The second set of data is a \newterm{data-product} called \code{noise_spec}, which is a part of the \spider main analysis pipeline.
%
Both of these data sets present challenges to use for this analysis: There is not much stare data, but on the other hand, \code{noise_spec} was not designed to model noise, but rather to create weights for channels in map-making.



In this section I describe these two choices, as well as the ancillary data I use: loadcurves and bias steps.



\subsection*{Stare data}



\spiderI's pre-flight testing resources were limited --- most receivers have not had dark runs to assess their in-lab noise levels.
%
This is a reasonable allocation of resources because the evaluations on the ground may not help us understand in-flight conditions, \ie, the RF interference, light levels, and grounding at float are drastically different from in the lab.
%
Instead, in-flight instrument performance is assessed with in-flight data acquired particularly for this purpose: \spiderI stopped scanning the sky for two short intervals and stared at a fixed point.
%
These two data sets do not need an estimated cosmic signal removed from them to examine the noise, the reaction wheel slows down, and any glint or source of scan-synchronous noise becomes static.



Of the two staring portions of the flight, the first, \code{stare_00}, is highly contaminated by known pathologies like \newterm{popcorn noise}.
%
The second attempt, \code{stare_01}, is cleaner, though bursts from the Iridium transmitter still disrupted the data, requiring cropping.
%
In this stare there are many fewer flagged pathologies, though some are still present at some level on some detectors, particularly in x3.



I cropped the event down to \code{stare_01_noise} which is the cleanest portion.
%
These twenty seconds are shown from an example detector in \myref{nse:sets}A.
%
During this time, the reaction wheel is still moving at $\mathcal{O}\left(10\right)$ degrees a second, but this does not seem to add significant noise.



\subsection*{\code{noise_spec}}



The \code{noise_spec} data-product, created by Anne Gambrel, was considered for this analysis.

This product was created by subtracting simulated cosmic signal from non-stare data and selecting small chunks of the resulting TOD that have no flags (so-called \newterm{golden micro-chunks}).
%
These micro-chunks are found throughout flight, and the noise spectra are averaged together to create \code{noise_spec}, benefiting from more data to get a more averaged PSD.
%
While this may be sufficient for weighing detectors in map-making, micro-chunks of different detector states (bias and resistance) are averaged together.
%
This makes it difficult to fit a detector model and if the detector's state can not be well constrained, the noise can not be well modeled.
%
In addition, \code{noise_spec} is noisier than \code{stare_01_noise} suggesting more pathologies eluded flagging in these golden micro-chunks than in the stare data.



Because of these limitations in \code{noise_spec}, I chose to continue with just \code{stare_01_noise}.



\subsection*{Loadcurves, bias steps, and values handed down}



A few hours before the stares, loadcurves were also taken to help characterize the detectors.
%
In the intervening few hours, FPU temperatures changed by as little as $0.6 mK$ (x2) and as much as $2.4 mK$ (x6), which should have no significant effect --- the loadcurves should provide good $R(bias)$ tables for the stare data sets.
%
The set of loadcurves from one of $x1$'s wafers is shown in \myref{nse:lcs}A.
%
Some aberrant loadcurves are apparent and I use a variety of heuristics to systematically remove unreliable detectors.
%
Cuts include detectors that have ($R_n < 20 m\Omega$) or ($R_n < 55 m\Omega$) which are too far from \spiderI's nominal $R_n = 32 m\Omega$.
%
I also cut $R(bias)$ curves that are sufficiently non-monotonic to be deemed misshapen.
%
In the loadcurves in \myref{nse:lcs}A, which also shows the same set of data in the $I(bias)$ and $R(bias)$ forms, red curves show loadcurves removed from the analysis.
%
The flight loadcurves that pass the cuts are used to create $R(bias)$ and $R_{dyn}(bias)$ tables to understand the state of the detectors during \code{stare_01_noise}.
%
Detectors with cut loadcurves are themselves removed from this analysis.



A set of bias steps was taken minutes before \code{stare_01_noise}.
%
The code written by other members of the \spider team which derives $R_{dyn}$ from bias steps for the main analysis is robust.
%
An example set of bias steps are shown in \myref{nse:sets}B.
%
Also available throughout flight is the thermometry log from the wafer NTDs, which gives us good estimates of $T_b$ used to calculate the heat flow between the TES islands and the bulk silicon of the wafers.



\begin{boxpairfigs}{\code{stare_01} data set and associated bias steps}{sn01_flaggings.png}{bias_steps.png}{nse:sets}
%
\begin{itemize}
%
\item[\bf A]%
%
The \code{stare_01} TOD from a randomly chosen detector.
%
Parts of this event is flagged (shaded regions, coinciding with RFI from Iridium pings);
%
The cleanest twenty seconds are clean and indicated in orange.
%
The other flags listed in the legend are identified in a minority of other detectors and not this randomly selected one.
%
\item[\bf B]
%
Bias steps taken three minutes before \code{stare_01} for the same detector.
%    
\end{itemize}
%
\end{boxpairfigs}



Some of the thermal properties of the detectors like $C$, $G$, and $T_c$ have been measured in the lab and should not have changed between the lab and flight, despite the time elapsed since those measurements.
%
However, these values are reported only as per-wafer averages in a document referred to here as the \newterm{tile stock sheet}.



In addition to the cuts already described, many detectors are labeled dead or frail by the main analysis and are also cut here.
%
I further cut detectors that have flags in the \code{stare_01_noise} data set. 



\begin{boxpairfigs}{Loadcurves and spectral scalars}{x1t1_lcs.png}{scalar_defs.png}{nse:lcs}
%
\begin{itemize}
%
\item[\bf A]%
%
In the top of this subplot are the $I(bias)$ curves and the derived $R(bias)$ curves from the same data set are in the bottom.
%
In both plots, red curves are deemed contaminated or too strange to be able to accurately determine detector parameter values, and those detectors are excluded from the analysis.
%
The uppermost $I(bias)$ curve has visible wiggles but was still included in the analysis, very close to but not exceeding the cut thresholds.
%
\item[\bf B]
%
One detector's \code{stare_01_noise} PSD.
%
The PSD is shown in black points.
%
The PSD is binned for fitting, shown in red bars with black bars indicating the local standard deviation around each bin.
%
Three summary scalars are measured from each PSD, named in the legend and marked as points, indicating the average noise in the bands shaded.
%
\end{itemize}
%
\end{boxpairfigs}



PSDs of \code{stare_01_noise} are computed, an example of which is shown in \myref{nse:lcs}B.
%
I pull three scalar values from each PSD which I call \code{low}, \code{sci}, and \code{exc}, corresponding to the averages of the PSD bins in the intervals $(0.1, 0.5) Hz$, $(1, 3) Hz$, and $(20, 35) Hz$, respectively.
%
These three numbers are sensitive to low-frequency noise, the band of frequencies at which \spider is most sensitive to the cosmic signal, and the noise bump at higher frequencies to which $m$-noise contributes.
%
While the final analysis in this project compares full PSDs, these three scalars are passed to an anomaly detection algorithm --- the \newterm{local outlier factor} method (LOF)%
\footnote{\href{https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.LocalOutlierFactor.html}{scikit-learn.org}\cite{SciPy:2020}} %
--- to detect detectors that likely have undiagnosed issues as shown in \myref{nse:anom}.



\begin{boxfig}{Outlier detection}{x1_anom.png}{nse:anom}
    
Scatter plots of the PSD scalars for x1.
%
The LOF method detects points that are not a part of any clusters, indicated by red crosses.
%
In this analysis, the LOF method catches detectors with too high \code{low} and \code{sci} noise, though the thresholds are different from FPU to FPU.
%
\end{boxfig}





\section{Fitting detectors}\label{nse:fit_det_models}



This analysis uses two sequential fits: first the detectors are fit to their $R_{dyn}(bias)$ found from bias steps, constraining the sharpness parameters, $\alpha_{I,0}$ and $\beta_I$ (the latter is a function of $I_c$ in the model).
%
The model being fit is the one described in \ref{dets}: a tanh transition for $R(I, T)$ and power-law thermal submodels.
%
$I_c$ and $\alpha_{I,0}$ are parameters in $R(I, T)$, seen in \myref{dets:ele_sub}, so this first fit constrains the shape of the transition.
%
This is done using my \EST codes which simulates TESs and can report the expected $R_{dyn}$ from a simulated detector.



The second fit uses \code{stare_01_noise} PSDs to constrain the readout plateau and $m$ which are described in \ref{nse:comp_models}.
%
This fit is a more straightforward $\chi^2$ fit.



This section discusses the first fit and the next section describes the noise modeling.



\subsection*{Fit procedure}



To fit the TES model, I maximize a posterior function which is the product of a likelihood and a few priors;
%
The likelihood is a gaussian where the maximum is reached when the model's bias steps agree with the measured bias steps in amplitude (equivalent to agreeing on $R_{dyn}$).
%
I use a prior of $\beta_I$ that is gaussianly distributed around zero with $\beta_I = 2.5$ at $1\sigma$.
%
The model has an explicit $I_c$ rather than $\beta_I$, but it is easier for the optimizer to take small steps in $\beta_I$ near zero instead of searching the unbounded $I_c < \infty$ space.
%
For $\alpha_{I,0}$'s prior, I use a gaussian distribution which peaks at $\alpha_{I,0} = 125$ with a wide uncertainty: $1\sigma = 50$.
%
From the loadcurve and bias steps, we have a good estimate on $R$, but I allow some wiggle-room, so I also add a prior which is a gaussian distribution centered around the previously estimated $R$ with a $1\sigma = 1 m\Omega$.



In addition to the priors and initial guesses, I put hard limits on the optimizer: $I_c$ must be within $(10^{-4}, 10^{4}) A$, $\alpha_{I,0}$ within $(50, 200)$, and $R$ within ($-1 m\Omega, 1 m\Omega$) of the nominal per-detector value.
%
The fit is considered a failure if the resulting model's $R_{dyn}$ is off from the measurement by more than $0.5 m\Omega$.
%
Detectors that fail to fit (are over-constrained) in this manner are cut from this analysis.



Adding more wiggle-room to more parameters drastically increases the computing time, which is already significant given that this analysis is repeated on hundreds of detectors.



\subsection*{Fit results}



After the cuts described in the section above (\ref{nse:data_sets}), only 460 detectors remained to be fit.
%
Of those, 278 failed the fit in the ways described in \ref{nse:fit_det_models}.
%
See table below for the per-FPU breakdown:



\begin{namebox}{First fit cuts}{nse:cuts1}
%
\begin{center}
%
\begin{tabular}{c|c|c|c}
%
FPU & before fit &- cut by fit & = after fit \\ \hline
x1 & 128 & 83 & 45\\
x2 & 63 & 29 & 34\\
x3 & 78 & 78 & \note{0}\\
x4 & 73 & 42 & 31\\
x5 & 13 & 12 & 1\\
x6 & 105 & 34 & 71\\ \hline
tot & 460 & 278 & 182
%
\end{tabular}
%
\end{center}
%
\end{namebox}



The x3 detectors are all eliminated which can be traced to a few contributing causes;
%
As has been previously noticed by the \spider collaboration, there is also a lot more noise and glitches n x3 data, and this can add error to the $R_{dyn}$ estimate from the bias steps, hindering the fit.
%
Another cause of poor fitting is inaccurate parameters fixed for the fit.
%
For example, $P_{opt}$, or the total loading, is only measured for x1 and x2 and extrapolated for the other FPUs by a ratio of gains, $dT_{sky}/dI$.
%
These $P_{opt}$ values are shared across FPU wafers even though we expect loading to vary from position to position on an FPU.



Despite these failings, the fits that passed give the best measurements for $\alpha_{I, 0}$ and $I_c$ currently available for these \spiderI devices.
%
Prior to this analysis, these values have never been well constrained for flight detectors and the priors are informed by prototype detectors that got shelved.



Surviving detectors have a models that strongly agree with real bias steps and most have parameter values in-line with expectations.
%
The model parameters are tightly bunched and do not fill the parameter space to the margins, suggesting these model succeeded beyond just survivor's bias, see \myref{nse:param_fit}.



\begin{boxpairfigs}{Fit parameters}{fit_alphaI0_v2.png}{fit_betaI_v2.png}{nse:param_fit}
%
These are stacked histograms: wafer histograms add to the height of the other wafer histograms. 
%
At this point in the analysis, all receivers have had at least an entire tile cut. 
%
\begin{itemize}
%
\item[\bf A] Fit values for $\alpha_{I,0}$.
%
\item[\bf B] Fit values for $\beta_I$.
%
\end{itemize}
%
In both subplots priors are superimposed as thin blue lines with arbitrary scaling.
%
Receivers x3 and x5 are excluded because of low or null detector counts.
%
\end{boxpairfigs}



\section{Fitting readout and excess noise}\label{nse:fit_nse_models}



\subsection*{Fit procedure}



Once I get a detector model fit to the bias steps, I compute the model's photon, phonon, and Johnson noise constributions.
%
As previously described, there are also $m$-noise and readout noise contributions.
%
This fit constrains those components by solving for $m$ and the readout plateau value, $\iota_{SQ}$. 



The usual $\chi^2$ test statistic which computes the difference between the predicted noise model and the PSD derived from data is minimized within the bounds:
%
$(10^{-12}, 10^{-9}) A/\sqrt{Hz}$ for the readout plateau and $(1, 20)$ for $m$.



Because PSDs natively have asymmetric error bars, I bin the PSDs with five points averaged per bin, before the comparison.
%
This increases the degrees of freedom per bin to yield a more gaussian distribution.
%
In binning I also compute the sample standard deviation per bin to use as weights on each contribution to the $\chi^2$.


\subsection*{Fit results}



No detector failed this fit because the PSDs have only two discriminating features: the height of the PSD at the \code{sci} band and the size of the excess noise bump in the \code{exc} band.
%
The location of the \code{exc} onset could be a feature too, but less distinctive in fitting the unexplained noise to either readout plateaus or $m$-noise.
%
There is some degeneracy between $m$ and the readout plateau, but $m$ is determined to within $\pm 1$.



The fits themselves are shown for a representative FPU in \myref{nse:pretty}.
%
The bump which is used to discriminate between $m$ contributions and the readout plateau is visible in the detectors and clearly absent in the DSs.
%
In \myref{nse:m_hist}A are the values of $m$ found in this fit.


\begin{boxfig}{Fit models}{pretty_spectra_NEI_x2_v2.png}{nse:pretty}
%
Example ensemble of fits for one FPU (x2).
%
In the left subplot are the model components fit for as described in the procedure above.
%
The percentile bands are found from the ensemble of detectors in x2, so the most opaque lines denote the median values for each model component for each frequency, the other lines follow the percentiles indicated in the title.
%
In the right subplot, the detectors' \code{stare_01_noise} PSDs are similarly treated as an ensemble for the same percentile lines to be drawn, as are the DSs of this FPU.
%
Imposed on top is the total model spectrum in magenta and a slight variation of this fit in green for comparison%
\footnote{These two fit methods yielded the same results so I do not describe the variation for brevity.}.
%
The variation of my fitting procedure found similar $\chi^2$ values but was less informative.
%
\end{boxfig}



The measured readout plateau distribution for detectors has a wider spread and fatter tail for higher plateau values than the spread computed from DSs.
%
Another view comparing DS noise against the fit readout plateaus is given in \myref{nse:m_hist}A which shows histograms for each wafer of each FPU with enough DSs: x2, x4, and x6.


\begin{boxpairfigs}{Fit parameters}{ds_vs_fit.png}{fit_m.png}{nse:m_hist}
%
\begin{itemize}
%
\item[\bf A]
%
A comparison of DS \code{sci} values and the derived model equivalent from the fit readout plateaus.
%
This plot is shown for x2, x4 and x6 because these FPUs have enough DSs.
%
Each FPU is split into column sets corresponding to wafers.
%
\item[\bf B]
%
The spread of fit $m$ values shown by stacked histogram.
%
As on may expect, $m$ values cluster by wafer.
%
The measurements on prototype devices of $m \sim 6$ or $7$ sit within the spread of $m$ values from this analysis.
%
\end{itemize}
%
\end{boxpairfigs}






\section{Discussion}



The stare data sets can help assess the noise performance of an experiment in an end-to-end fashion that can not be replicated in the lab.
%
Moreover, stare data eliminates systematics and sky signals that are caused by scanning.
%
The incidental modeling helps us better know our detectors which have poorly constrained parameters.



The RF sensitivity is an obstacle in measuring the underlying detector and readout noise model, so instead I focus on better understanding the RF, in particular:
%
does RF generated noise come from TESs absorption?



The second fit of this analysis separates the excess noise into the $m$ component and the readout plateau, which helps break the degeneracy of where RF noise enters the system by separating noise into readout and TES-like components.
%
But to conclude if these results are significant and to verify that this disentanglement works as I propose, we need more data.
%
We have already tried beaming RF at an insert in \stacy, but previous attempts to induce RF noise in the lab focused on trying to direct radio waves at either the MCE or down the telescope tube to try to suss out the susceptible entry point.
%
However, in both cases detector noise increased and it was never clear if we successfully shielded one entry point as we fed signal into the other.
%
If such lab data is analyzed in a similar manner to this project, we can disentangle increases in $m$-noise from increases in $\iota_{SQ}$, adding an analysis dimension to previously difficult lab work.



In flight, we could take stare data at a high-sample rates; Data taken at $50 MHz$, $250 kHz$, or even just $15 kHz$ would have less aliasing and perhaps even clear features for fits and to discover which frequencies are affected by RF interference.
%
However reconfiguring the MCE by automation in flight risks failing to get back into the normal operating state thus is often rejected in the cost-benefit analysis.
%
Repeating this process for \spiderII data would test my findings and procedures for the re-flown detectors and test the new detectors in the 280s which are made with a different superconducting material and have feedhorns which reflect a lot of RF.



Having additional stare time occur periodically, \eg every few days, would allow detectors to be examined in multiple states and conditions, but needs to be weighed against the cost of precious flight time.
%
An alternative is to fit for detector state associated with each good chunk of data so that aggregated fits can be computed.
%
The \spiderI bug in the bias step acquisition code that disabled bias steps in part of the flight was caught and fixed after the issue was discovered.
%
In \spiderII, we can reliably compute TES states, which will enable flight-wide noise analyses.
%
For example, repeating this analysis at different pointings could provide clues on the source of any YSSN.









A well functioning noise model that can correctly predict the noise could improve map-making by assigning different weights to different chunks of data.
%
Instead, the \code{noise_spec} data product gives a flight-long estimate of noise per channel and chunks are weighted by these static values.



The detector model that can correctly predict the gain of detectors, the ratio $dT_{sky}/dI$, has been elusive: \spiderI detectors are calibrated by comparing $T$ maps with \planck and this calibration does not agree across all detectors with detector $dP_{opt}/dI \times dT_{sky}/dP_{opt}$ where the second factor is the optical efficiency of detectors which can be measured in the lab with a source of known brightness. 
%
Improving our understanding of detector parameters can help both approaches agree.




