\begin{thebibliography}{1}



\bibitem{chapter:Irwin_Hilton:TES:2005}
K.~D Irwin and G.~C. Hilton.
\newblock Transition-edge sensors.
\newblock In Christian Enss, editor, {\em Cryogenic Particle Detection},
  chapter~3, pages 63--150. Springer, Heidelberg, Berlin, Germany, 2005.

\bibitem{thesis:Rahlin:2016}
A.~Rahlin.
\newblock {\em The First Flight of the SPIDER Balloon-borne Telescope}.
\newblock PhD thesis, Princeton University, 2016.

\bibitem{Hubmayr:2018}
J.~Hubmayr, et~al.
\newblock Low-temperature detectors for cmb imaging arrays.
\newblock {\em Journal of Low Temperature Physics}, 193(3–4):633–647,
  Springer Science and Business Media LLC, Aug 2018.

\bibitem{Johnson:1928}
J.~B. Johnson.
\newblock Thermal agitation of electricity in conductors.
\newblock {\em Phys. Rev.}, 32:97--109, American Physical Society, Jul 1928.

\bibitem{Fraser:2004}
G.~W. {Fraser}.
\newblock {On the nature of the superconducting-to-normal transition in
  transition edge sensors}.
\newblock {\em Nuclear Instruments and Methods in Physics Research A},
  523(1-2):234--245, May 2004.

\bibitem{Lindeman:2006}
{M. A.} Lindeman, et~al.
\newblock Percolation model of excess electrical noise in transition-edge
  sensors.
\newblock {\em Nuclear Instruments and Methods in Physics Research, Section A:
  Accelerators, Spectrometers, Detectors and Associated Equipment},
  559(2):715--717, Elsevier, April 2006.

\bibitem{Wessels:2019}
Abigail Wessels, et~al.
\newblock A model for excess johnson noise in superconducting transition-edge
  sensors, 2019.

\bibitem{SciPy:2020}
Pauli Virtanen, et~al.
\newblock {{SciPy} 1.0: Fundamental Algorithms for Scientific Computing in
  Python}.
\newblock {\em Nature Methods}, 17:261--272, 2020.

\end{thebibliography}
