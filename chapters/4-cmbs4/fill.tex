\chapter*{Chapter 5\\\cmbsIV Detector Design}
\addcontentsline{toc}{chapter}{Chapter 5: \cmbsIV Detector Design}
\def \chapterlabel {5}
\label{cmbs4}
\setcounter{eqnum}{1}\setcounter{fignum}{1}



\begin{introfig}{horn_array.jpg}{cmbs4:intro_pic}                     
%
A feedhorn array.
%
Photo reproduced from \cite{cmb-s4.org:det_mods}. 
%                                                                       
\end{introfig}                                                          
\clearpage   



\cmbsIV%
\footnote{\href{https://cmb-s4.org/}{cmb-s4.org}}% 
\cite{CMBS4:2017:tech, CMBS4:2019:science} is the next generation ground-based CMB observing program.
%
This program is largely funded by the Department of Energy (DOE) and the NSF.
%
The DOE has a long-standing interest in particle physics to which \cmbsIV will contribute directly by constraining possible inflatons (primarily by constraining $r$), as well as in constraining the \newterm{effective number of neutrinos} ($N_{eff}$) and the sum of neutrino masses.
%
The NSF has supported many cosmological studies: \cmbsIV will contribute to our understanding of our cosmos by complementing surveys like those done by the Large Synoptic Survey Telescope (LSST) \cite{Anthony:2002}, the Dark Energy Spectroscopic Instrument (DESI) \cite{DESI:2014}.
%
The resulting synergistic analyses can better measure parameters like $H$, and add constraints on dark matter and dark energy models.



The improvement over past stages is shown in \myref{cmbs4:stages}.
%
With a planned half-million detectors distributed over twenty-one telescopes, it is critical for \cmbsIV's science goals to achieve excellent detector performance.
%
However, with a handful of iterable parameters per detector and several types of detectors sharing similar readouts, simulation work is required to constrain the space to be explored by prototypes.



\begin{boxfig}{Stage 4 versus stages 2 and 3}{stages_timeline.png}{cmbs4:stages}
%
A timeline showing the expected improvements in sensitivity ($\mu K^2$) and cosmological parameters including the targets for \cmbsIV.
%
Reproduced from \cite{CMBS4:2016:science}.
%
\end{boxfig}



The work in this chapter is not full detector optimization;
%
Too little is known about what the final configurations will look like, including the excess noise in the TESs, the SQUID noises, and the optical loading measured in receivers once they are built.
%
The process will take a while for optimized detector parameters to be confirmed and implemented.
%
The immediate goal of these simulations was to inform the readout team of the practicality of adopting TDM across the program over alternative multiplexing schemes. 
%
This project is also made to be very iterable.
%
The code is easy to alter as the foundries provide feedback.



The model used in this chapter is described in \ref{dets} and \ref{nse:comp_models}.
%
In this chapter I describe the constraints and specifications in \ref{cmbs4:constraints}.
%
The parameter search is detailed in \ref{cmbs4:search}, where I also discuss my selection for TES parameters that act as good placeholders before trial-and-error gives us more information.



\section{\cmbsIV design}\label{cmbs4:constraints}



\cmbsIV will use two basic telescope designs: \newterm{Small aperture telecopes} (SATs) will look for $B$-mode at $\ell \lesssim 200$.
%
\newterm{Large aperture telescopes} (LATs) will observe at angular scales as high as $\ell \sim 10,000$ (resolving arcminute-scale features), which will yield lensing studies, and observations of astronomical objects like galaxies and galaxy-clusters.



While \newterm{stage-3} CMB experiments are disparate experiments which each feature $\mathcal{O}(10^4)$ detectors, \cmbsIV unifies efforts to observe the sky with hundreds of thousands of detectors on $100 mK$ FPUs%
\footnote{Achieved by dilution firdges.} 
across several observing sites.
%
Several foundries will contribute detector production, including NIST, JPL, Argonne National Laboratories (ANL), and Lawrence Berkeley National Laboratory (LBNL) partnered with SeeQC%
\footnote{A private enterprise, see: \href{https://seeqc.com/}{seeqc.com}}.
%
Designing detectors requires design constraints and specifications and feedback from foundries --- detector fabrication groups sometimes referred to here as \newterm{fabs} --- about the range of values some parameters can take.
%
With such a large umbrella program as \cmbsIV, the research and development needed to achieve the technology to enable such a high detector count is streamlined and technology choices are applied across the participating groups.



One critical decision was which multiplexing technology to use to read out so many detectors.
%
Because of my understanding of \spider's time-division multiplexer, I took on the task to see if \spider-similar detectors and a readout with twice as many channels could meet \cmbsIV's needs.



I only focus on the small aperture instruments, though expanding the project to the large apertures is straightforward.
%
These receivers will come in four polarized dichroic configurations such that each feedhorn directs light to four detectors: a pair of detectors per orthogonal polarization and two frequency sensitivity bands per pair. 
%
This dichroic scheme allows for more maps at different frequencies without needing to install many more receivers.
%
These dichroic wafers will observe at $(30\ \&\ 40)GHz$, $(85\ \&\ 145)GHz$, $(95\ \&\ 155)GHz$, and $(220\ \&\ 270)GHz$.



For each band, an optical loading has been estimated and safety factor of $2.5$ specified.
%
This specifies a target Joule power ($P_J = 2.5 \times P_{opt}^{expected}$) which accommodates variation in loading.
%
In the event of poor atmospheric conditions, the detectors should still be able to get on transition by operating with lower current and maintain their responsiveness.
%
This specification constrains each detector's operating $G$.



Other parameters depend greatly on each foundry's techniques, \eg $\alpha_{I,0}$ depends on TES film composition.
%
We use $\alpha_{I,0} = 75$ as a placeholder.
%
At $R/R_n = 50\%$ I use $\beta_I = 0.5$, which will determine a $I_c$.
%
The target operating resistance is $R = 8 m\Omega$, so I use $R_n = 16 m\Omega$
%
This $R$ and $R_n$ is flexible and I was encouraged to explore other options to see if lower or higher resistances have advantages%
\footnote{Lower resistances are easier to multiplex.}.



I use the exponents $\beta_G = 2.5$ and $\beta_C = 1$ to model $G(T)$ and $C(T)$ as described in chapter \ref{dets}.
%
The remaining parameters are $C$ and $L$, giving us two knobs to turn to tune the time-constants $\tau_+$ and $\tau_-$.
%
The detectors need to be stable and overdamped, as described in \ref{dets:tes_behave}, for which $\tau_\pm$ must be positive and real.
%
We also need $\tau_-$ to be fast such that the maps are not blurred by the scanning, particularly for the higher frequency instruments.
%
These higher frequency instruments have smaller beams and so for the same scan pattern, will move more beam-widths in an interval.
%
My target upper limits for $\tau_-$ are given in \myref{cmbs4:taus}.



Another concern is the \newterm{aliasing penalty}, defined as the ratio of the detector noise in a plausible science band ($1-3 Hz$) before and after aliasing%
\footnote{This calculation is done without the readout noise on either side of the fraction.}%
.
%
We impose that the detector aliasing does not add more than $1\%$ (aliasing penalty $< 1.01$) noise, which effectively acts as a constraint on $\tau_+$: if too fast, too much detector noise is past the Nyquist frequency, but if $\tau_-$ is fast and $\tau_+$ is too slow, the detector noise forms a high peak which can increase aliasing.



The proposed time-division system multiplexes over sixty-four rows --- nearly twice as many --- using new two-level switch technology \cite{Dawson:2019}%
\footnote{Since the completion of this project the readout team has changed the target to eighty rows.}.
%
When switching rows, we need at least five time-constants to pass for the signal to settle for each datum to be recorded cleanly.
%
Using \spider's demonstrated $15 kHz$ sampling rate would require SQUIDs with $1.6 MHz$ bandwidths --- as another placeholder I assign $2.5 MHz$ to \cmbsIV SQUID bandwidths.
%
If the detector aliasing penalty is too high, pushing towards the higher sampling frequency would be higher priority.
%
However if $15 kHz$ is sufficient, the \cmbsIV team could add more rows to fully utilize the SQUID bandwidth, or filter the SQUID down to a $1.6 MHz$ bandwidth to reduce noise aliased into the data.
%
Even for the fastest detectors with $\tau_- < 1 ms$, the slower $15 kHz$ sampling rate gets fifteen samples per time constant, which means this readout can resolve any signal picked up by the instrument. 



So far our requirements are: hard time constant bounds as described in \myref{cmbs4:taus}, requiring stability and overdampedness (let's call the combination \newterm{viability}), and not too much detector aliasing for our fiducial sampling rate of $15 kHz$.
%
Additionally, these conditions need to be met over a decent portion of the transition.
%
Bias lines are shared among many detectors, and detectors are not all uniform; 
%
This means we expect some scatter of $R/R_n$ state in each biased column.
%
Viability over more of the transition also leads to better robustness and ensures flexibility in the bias points, as sometimes we discover additional benefits to biasing away from $R/Rn = 50\%$ (see \ref{mono:step_miti}).
%
To ensure this, I model noise on many points on the transition: $R/R_n \in \{ 30\%, 40\%, 50\%, 60\%, 70\%, 80\% \}$.
%
In addition to the concerns over detector uniformity, vetting all of these transition points also ensures excursions from the set-point, presumably $R/R_n = 50\%$, whether by a muon interaction or a quick variation in atmospheric conditions, will not cause undesirable detector behavior.
%
While undesirable, some underdamped behavior can be tolerated at very high biasing fraction ($R/R_n > 0.7$), so long as the detector is stable for all $R/R_n \geq 30\%$.




\begin{namebox}{Limits on $\tau_-$}{cmbs4:taus}
%
\begin{center}
%
\begin{tabular}{c|c||c|c}
%
frequency $[GHz]$ & $\tau_- <$ [ms] & frequency $[GHz]$ & $\tau_- <$ [ms] \\ \hline
%
30 & 4 & 145 & 1.5 \\
%
40 & 4 & 155 & 1.5 \\
%
85 & 1.5 & 220 & 1 \\
%
95 & 1.5 & 270 & 1 \\
%
\end{tabular}
%
\end{center}
%
Initial upper limits on $\tau_-$ for each frequency detector shared internally.
%
Higher frequency detectors have more stringent limits due to smaller beam sizes.
%
\end{namebox}



Example spectra for a modeled detector are given in \myref{cmbs4:eg_spectra}.
%
Each of the three subplots is the same model but at different bias points, showing how the noise changes --- in this case forming a more pronounced peak --- as the time constants approach each other for lower $R/R_n$.



\begin{boxfig}{Example spectra}{145GHz_spectra.png}{cmbs4:eg_spectra}
%
Noise spectra from a simulated $145 GHz$ detector.
%
The peak which forms when $\tau_\pm$ approach each other is indicated by a red arrow, but it relatively modest in this case.
%
\end{boxfig}



\section{Parameter search}\label{cmbs4:search}



I execute a search in $(R_n, L, C)$ to find which combinations yield viable detectors that meet our criteria.
%
Each $(R_n, L, C)$ is inputted into the models described in chapters \ref{dets} and \ref{nse}.
%
Detectors that are not valid on enough of the transition, have aliasing penalties greater than $1.01$ at $R/R_n = 50\%$, or have time constants greater than the per-band limits, are dropped, leaving only \newterm{suitable} detectors.



From the set of suitable detectors, I identify a range for $C$ that can work for both bands sharing dichroic wafers%
\footnote{Both bands on a wafer sharing the same heat capacity simplifies the masks required for fabrication.}.
%
Those parameter spaces are shown in \myref{cmbs4:LandC}.
%
For each choice of $R_n$, bands of suitable $C$ values are shown, though they largely overlap.
%
The suitable $L$s then depend on both $C$ and $R_n$.
%
Generally lower values are preferred, but $L$ is limited by the stray inductances of the experiment's wiring ($L \gtrsim 0.2 \mu H$).



\begin{boxfig}{Constraining $L$ and $C$}{check1.png}{cmbs4:LandC}
%
Parameter choices that yield suitable detectors.
%
Here $L$ and $C(T_c)$ are iterated as grids, plotted for three choices of $R_n$: $R_n = 8 m\Omega$ (blue), $R_n = 12 m\Omega$ (green), and $R_n = 16 m\Omega$ (purple).
%
The bands indicating $C$ values which work for both frequencies on each dichroic wafer overlap such that they come out as single bands in each subplot.
%
In larger points are selections for $L$ and $C(T_c)$ I use to further explore these parameters, with one point per band and $R_n$ combination.



The higher the frequency, the smaller the suitable parameter space.
%
\end{boxfig}



I chose central values of suitable $L$ and $C$ for each $R_n$ and band to make \myref{cmbs4:RonRn}, which shows the behavior of each configuration at different bias points.
%
From this figure we can discern that lower $R/R_n$ biasing points have lower aliasing penalty but in some cases approach the underdamped regimes, \eg $145 GHz$'s $R/(16 m\Omega) = 80\%$.
%
At higher bias fraction aliasing penalty increase, often exceeding our threshold for higher $R_n$ at $R/R_n = 80\%$.
%
This puts pressure for lower $R_n$ than the prior specification of $R_n = 16 m\Omega$.
For a table of all my recommended values, see \myref{appn:extras:LandCs}.



\begin{boxfig}{Checking different $R/R_n$ values}{check2.png}{cmbs4:RonRn}
%
Detector models are generated for each ($L$, $C(T_c)$) picked out in \myref{cmbs4:LandC}.
%
For each band, the six biasing points form a column.
%
This is because the bias point does not change $L$, but does change $T$ and therefore $C$.
%
The $R_n$ for each column is indicated in text at the top of each column.
%
The bias point is indicated for the $30 GHz$'s $R/(8 m\Omega) = 80\%$ and $R/(8 m\Omega) = 30\%$, but the vertical order is identical for each column and in each subplot.
%
The detector aliasing penalty is shown by point color, as indicated with the color-bar.
%
\end{boxfig}



\section{Further work}



This simulation work provides a range of values for $C$, and the identified pressures for lower $R_n$ and $L$.
%
Several placeholder values were used (like the choice of $\alpha_{I,0} = 75$) but these inputs are easily updated as data from prototypes become available.
%
Though this means my provided values are subject to change, they have been passed to the \cmbsIV readout team.
%
This analysis was used by the collaboration during the 2019 \cmbsIV down-select process to pick TDM as the experiment's multiplexing technology over alternatives.



This project has been continued by Jeff Filippini as the \cmbsIV design matures, and as more information is relayed between the detector, readout, science,  and other teams.
%
As already footnoted, the baseline multiplexer has been expanded to handling eighty rows instead of sixty-four.
%
$R_n$ of $12 m\Omega$ is now preferred.



At this early stage of \cmbsIV we are waiting on more prototypes and prototype characterization.
%
This process will be slow, but device characteristics are important for other groups in the collaboration to design their components.
%
Modelling like that described in this chapter helps prevent roadblocks in detector development from spreading to the multiplexing team.
