\begin{thebibliography}{10}



\bibitem{Planck:X:2013}
P.~Ade, et~al.
\newblock Planck2013 results. x. hfi energetic particle effects:
  characterization, removal, and simulation.
\newblock {\em Astronomy \& Astrophysics}, 571:A10, EDP Sciences, Oct 2014.

\bibitem{Suzuki:2018}
A.~Suzuki, et~al.
\newblock The litebird satellite mission: Sub-kelvin instrument.
\newblock {\em Journal of Low Temperature Physics}, 193(5-6):1048–1056,
  Springer Science and Business Media LLC, May 2018.

\bibitem{Osherson:2020}
B.~Osherson, et~al.
\newblock Particle response of antenna-coupled tes arrays: Results from spider
  and the laboratory.
\newblock {\em Journal of Low Temperature Physics}, 199(3-4):1127–1136,
  Springer Science and Business Media LLC, Mar 2020.

\bibitem{Miniussi:2014}
A.~Miniussi, et~al.
\newblock Study of cosmic ray impact on planck/hfi low temperature detectors.
\newblock {\em Journal of Low Temperature Physics}, 176(5-6):815–821,
  Springer Science and Business Media LLC, Feb 2014.

\bibitem{Catalano:2014a}
A.~Catalano, et~al.
\newblock Impact of particles on the planck hfi detectors: Ground-based
  measurements and physical interpretation.
\newblock {\em Astronomy \& Astrophysics}, 569:A88, EDP Sciences, Sep 2014.

\bibitem{Catalano:2014b}
A.~Catalano, et~al.
\newblock Characterization and physical explanation of energetic particles on
  planck hfi instrument.
\newblock {\em Journal of Low Temperature Physics}, 176(5-6):773–786,
  Springer Science and Business Media LLC, Feb 2014.

\bibitem{Haller:1996}
E.~E. {Haller}, K.~M. {Itoh}, and J.~W. {Beeman}.
\newblock {Neutron Transmutation Depot (NTD) Germanium Thermistors for
  Submillimetre Bolometer Applications}.
\newblock 388:115, December 1996.

\bibitem{Planck:IV:2011b}
P.~Ade, et~al.
\newblock Planck early results. vi. the high frequency instrument data
  processing.
\newblock {\em Astronomy \& Astrophysics}, 536:A6, EDP Sciences, Dec 2011.

\bibitem{Hanany:2019}
S.~Hanany, et~al.
\newblock Pico: Probe of inflation and cosmic origins, 2019.

\bibitem{Tanabashi:2018}
M.~Tanabashi et~al.
\newblock {Review of Particle Physics}.
\newblock {\em Phys. Rev. D}, 98(3):030001, 2018.

\bibitem{Asai:2013}
Makoto Asai and Richard Mount.
\newblock Geant4 - current and future : A snowmass 2013 white paper, 2013.

\bibitem{DeKorte:2003}
Piet DeKorte, et~al.
\newblock Time-division superconducting quantum interference device multiplexer
  for transition-edge sensors.
\newblock (74), Review of Scientific Instruments, August 2003.

\end{thebibliography}
