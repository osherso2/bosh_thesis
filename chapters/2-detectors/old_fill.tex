\chapter{TESs and MCEs}
\def \chapterlabel {3}
\label{dets}

This chapter provides background for Transition Edge Sensor (TES) based detectors.
%
All of the work in this thesis features discussion of some TES behavior or other \red{summarize next chapters with refs}.
%

\red{describe structure of this chapter}

\section{Why use TESs?}

To justify the proliferation of TESs in modern CMB experiments, first I describe the Planck NTD then TES similarities and competitive dissimilarities.

\red{Consider the NTD}

\red{TES advantages over NTDs / justify TESs}

TESs are films of superconductors held on their phase transition.
%
Because of the well-known ``sharpness'' of a superconductor's transition, it is clear that once such a film can be \newterm{on-transition}, it can under-go large changes in resistance from a small change in heat.
%
In other words, a superconductor on-transition is a promising thermistor.

\red{Explain in previous chapter why we need more detectors (instead of just very sensitive ones).}

\red{describe a BIT about how TESs are multiplexed, which takes advantages of SQUID amplifiers. Need to bring up the offset.}

\section{Physical descriptions of TESs}

First I describe the physical components to a detector with TES technology using the SPIDER 1 detectors as an
example.
%
This includes the basic governing physics equations resultant from the detector design.
%
Then I detail the model which is linearized to better understand TES behavior.
%
That linearized model is used in proceeding sections to derive stability criteria and more \red{flesh this out}.

As previously discussed, modern experiments use TESs that are lithographically fabricated in wafers.
%
A close up of such a TES --- one of SPIDER's --- is shown in \myref{isl}.
%
A few particularities are immediately visible:
\begin{itemize}
\item The detector is comprised of a \newterm{island} of wafer substrate suspended over an etched-out canyon by zig-zagged \newterm{legs}, allowing the substrate on which the TES is patterned to have a significant temperature difference from the bulk of the wafer.
%
Leads patterned onto the right leg brings the biasing voltage onto the island.
%
\item SPIDER has two TESs per island, one made out of titanium with a low critical temperature, and one made out of aluminum with a high critical temperature, at about $500 mK$ and $1.5 K$ respectively.
%
\item The structures seen off-island are slot-antennas.
%
Power absorbed by one orientation of antennas flows to the resistive gold meander on the island by the waveguides patterned onto the pictured left leg.
\end{itemize}
\red{Check Tcs}
\red{Give dimensions}
\red{Give dimensions for traces}
\red{Cite Hannes' work}
\red{Mention the interlacing}

\begin{boxfig}{A SPIDER TES up close}{TES_island_magnified.jpg}{isl}
A magnified picture of one of SPIDER's TES detectors.
%
The central structure is formed by etching out a ``canyon'' from the wafer substrate leaving the legs and island on which the TESs are patterned.
%
Indicated with $(1)$ is the titanium TES.
%
Indicated with $(2)$ is the aluminum TES. Finally $(3)$ indicates the resistive gold meander.
\red{Cite where picture is from.}
\red{Give dimensions here again. Or add it to the image.}
\end{boxfig}

When SPIDER is deployed high in the atmosphere, the focal planes are only illuminated by the faint galactic signal of interest and the TES islands can cool to the titanium TES's critical temperature.
%
In these conditions, the aluminum TES is just a superconductor and can be ignored when considering detector dynamics.
%
However, it can be difficult to reproduce the dark environment of flight in the lab and consequentially difficult to keep the titanium TESs on-transition in testing.
%
This is why we have the aluminum TES in series.
%
Because it shares the same island as the titanium TES (also known as the \newterm{science TES}), we can use the aluminum TES to characterize properties of the lithographic structures, as well as the optical chain which is agnostic to which TES is on-transition.



By interlacing slot-antenna arrays with orthogonal polarization sensitivities onto the same space, a pair of detectors can be identified with nearly the same beam extending between the sky and the focal plane, but split into two polarizations.
%
We call these detector pairs \newterm{A/B pair}s.
%
Some \red{high?low?}-pass filters are incorperated in the waveguides between the antennas and the meander to help shape the band in frequency space.
%
These filters may be visible on close inspection of \myref{isl}: They are little rectangles near each slot antenna.

\red{now describe array architecture}

\section{Thermal and electrical model for the TES}

The thermal model we use to understand TES behavior \red{CITE I\&H} is diagrammed in \myref{thm_dia}.
%
The bulk of the wafer is at a base temperature, \newterm{$T_b$}, and has a large enough heat capacity and internal thermal conductivity so that $T_b$ varies slowly relative to TES time constants, and is fairly uniform across the wafer.



The combined leg thermal conductivity, $G$, is low enough for the island temperature, $T$, to reach nearly the TES critical temperature, $T_c > T_b$, but high enough power causing $T > T_c$ to quickly dissipate from the island, which also depends on the island heat capacity, $C$.
%
For simple modeling, the island and its components are treated as isothermal.

\begin{boxfig}{Thermal model}{tes_thm_model.pdf}{thm_dia}
The thermal model for a TES: The island and all of its constituents are treated as isothermal with a heat capacity, $C(T)$.
%
The only path for heat to leave the island is through the combined leg thermal conductivity, $G(T, T_b)$.
\end{boxfig}

Power flows into the island from two sources: From power dissipated by the gold meander which is the \newterm{optical loading}, and from Joule heating of the TES's own bias current.
%
Power is removed by heat flow along the legs.
%
The balance of these contributions leads to the equation in \myref{thm_eq}.

\begin{namebox}{Thermal equation}{thm_eq}
Temperature of the island changes with time in accordance with:
%
\begin{align*}
C \frac{dT}{dt} &= P_{opt} + I^2 R - P_{legs}
\end{align*}
%
Where $P_{opt}$ is the optical loading and $P_{legs}$ is the power that dissipates through the legs, $I$ is the current flowing through the TES which has resistance $R$.
\end{namebox}

A TES is voltage biased and its resistance, $R$, is read out by the change in current.
%
Off-wafer there is an inductor, a shunt resistor, and biasing components: a voltage source behind a bias resistor to supply a bias current which is then passed to the shunt and TES in parallel to provide an island voltage.
%
These features are in \myref{ele_dia}.

\begin{boxfig}{Electrical model}{tes_ele_model.pdf}{ele_dia}
A voltage source, called the bias digital to analogue converter or bDAC, puts out a voltage, $V_b$, behind a large bias resistor of resistance $R_b$ such that a bias current $I_b \approx V_b/R_b$ biases the parallel detector circuit.
%
The resistance of the TESs on the island is $R$.
%
$R_{sh}$ is the shunt resistance, and $L$ is the inductance that couples the TES current to a superconducting-quantum-interference-device (SQUID) not shown here.
%
The dotted segment of the line indicates that there can be many of these detector circuits biased by a common $I_b$.
\end{boxfig}

To analyze the electrical behavoir, we can reduce the circuit to its Th\'evenin equivalent which is shown in \myref{ele_thev}.
%
Using this simplified circuit, the balancing equation is in \myref{ele_eq}.
%
\red{Put derivation / define Thevenin equivalent in appendix}

\begin{boxfig}{Electrical Th\'evenin equivalent}{tes_ele_thev_model.pdf}{ele_thev}
The Th\'evenin equivalent circuit for \myref{ele_dia}.
%
These new variables are defined as:
\begin{gather*}
V_{th} \equiv \frac{V_b R_{sh}}{R_b + n R_c} \sps %
R_{th} \equiv R_{sh}, \\
\text{where } R_c(\omega) \equiv \frac{R_{sh}\left( R(\omega) + \omega L \right)}%
{R_{sh} + R(\omega) + \omega L}
\end{gather*}
Where I approximate that all $n$ detectors biased by $I_b$ have identical $R$, $R_{sh}$, $L$.
%
Because $R_b$ is chosen to be much larger than $R_c$, $R_c$ can be ignored in $V_{th}$, but I keep it in for now.
\end{boxfig}

\begin{namebox}{Electrical balance equation}{ele_eq}
\begin{gather*}
V_{th} = \left(R_{th} + R\right) I + L \frac{dI}{dt}
\end{gather*}
\end{namebox}

I like to think of the \newterm{state of the TES} as a point in $(I, T)$ space.
%
That is to say, once the device parameters are set, e.g. the island is made with a particular thickness of silicon-nitride which determines its heat capacity, then for a bias voltage, $V_b$, wafer temperature, $T_b$, and loading, $P_{opt}$, the TES will reach an island current and temperature, $(I, T)$ which determines its resistance, $R$.



To model TES behavior, we need a few sub-models for $R$ and $G$.
%
Any sigmoid function can imitate $R(I, T)$'s sharp transition --- I use a tanh function --- so long as ``sharpness parameters'' can be computed.
%
For $G$, we use a simple power law.
%
These sub-models and new parameters are defined and expanded on in \myref{sub_mdl}.
%
\red{Cite Sasha as source for this sub-model.}

\begin{namebox}{Sub-models}{sub_mdl}
To model leg thermal conductivity:
%
\begin{gather*}
P_{legs} = \int_{T}^{T_b} G(\tilde{T}) d\tilde{T} \sps 
G(T) = G_x\left(\frac{T}{T_x}\right)^{\beta_G}
\end{gather*}
%
Where I usually use $T_x = 450 mK$ as a reference temperature so that $G_x = G(450 mK)$.
%
A derivation of $P_{legs}$ from Fourier's law of heat conduction is in \red{make appendix}.



To model the resistance:
%
\begin{gather*}
R(I, T) = \frac{R_n}{2}\left( 1 + tanh\left( \frac{T - T_c\left(1 - %
\left(I/I_c\right)^{2/3}\right)}{T_c / \alpha_I^0} \right) \right)
\end{gather*}
%
In this sub-model the normal resistance, $R_n$, critical temperature, $T_c$, and critical current, $I_c$, explicit.
%
The two ``sharpness parameters'' which characterize the transition's sensitivity to temperature and current are $\alpha_I$ and $\beta_I$, respectively, and are defined as the dimensionless quantities:
%
\begin{align*}
\alpha_I &\equiv \frac{T}{R}\frac{dR}{dT}
= 2\alpha_I^0\left(1 - \frac{1}{R_n}\right)\left( \frac{T}{T_c} \right)\\
\beta_I &\equiv \frac{I}{R}\frac{dR}{dI}
= \frac{4}{3}\alpha_I^0\left(1 - \frac{R}{R_n}\right)\left(\frac{I}{I_c}\right)^{2/3}
\end{align*}
%
In this model, $\beta_I^0 = \frac{2}{3}\alpha_I^0$, but in regular operation, $\beta_I \ll \beta_I^0$.
\end{namebox}

Detectors stay on-transition because of \newterm{electro-thermal-feedback}.
%
When the optical loading fluctuates by a small amount, $\delta P_{opt}$, the island temperature also fluctuates $\delta T$, which causes a significant $\delta R$.
%
Because the TES is voltage biased, the resultant current fluctuation, $\delta I$, varies with the opposite sign as $\delta R$ and $\delta P_{opt}$.
%
This creates a negative feedback: The Joule power, $\delta P_J$, fluctuates in the opposite direction as $\delta P_{opt}$.
%
To better understand this process, I linearize the coupled differential equations in \myref{tes_thm_linear} and \myref{tes_ele_linear}.
%
In these boxes, I consider a small change in the state of the TES, $I = I_0 + \delta I,\ T = T_0 + \delta T$, and propagate these small variations to $R = R_0 + \delta R$ and each of the power balance terms.
%
However, I do not expand $\alpha_I$, $\beta_I$, $G$, and $C$ because these do not change at the same significance.

\begin{namebox}{Linearized TES thermal behavior}{tes_thm_linear}
Suppose we consider small changes in $I$ and $T$ away from the point $(I_0, T_0)$.
%
Expanding $R$ to first order we get:
%
\begin{align*}
R &= R_0\left(1 + \alpha_I\frac{\delta T}{T_0} + %
    \beta_I \frac{\delta I}{I_0} \right) \\
P_J &= P_{J,0} \left(1 + \alpha_I \frac{\delta T}{T_0} + %
    \beta_I \frac{\delta I}{I_0} + 2I_0 R_0 \delta I \right) \\
P_{legs} &= P_{legs, 0} \left( 1 + G_0 \delta T \right)
\end{align*}
%
Suppose this change in $I, T$ is accompanied by an impulse of heat to the island, $\delta Q$.
%
The linear thermal balance equation is then:
%
\begin{gather*}
C\frac{d\delta T}{dt} = \delta Q - G(1 - \mathcal{L}_I) \delta T %
+ I_0 R_0 \left(2 + \beta_I \right) \delta I %
\text{ where } \mathcal{L}_I \equiv \frac{P_{J, 0} \alpha_I}{G T_0}
\end{gather*}
%
The quantity $\mathcal{L}_I$ is called the \newterm{loop gain} and its significance is shown in \red{AN APPENDIX}.
\end{namebox}

\begin{namebox}{Linearized TES electrical behavior}{tes_ele_linear}
Given the same small change in $I, T$ as in \myref{tes_thm_linear}, but including a possible $\delta V$, the electrical balance becomes
\begin{gather*}
L \frac{d\delta I}{dt} = \delta V - R_{th}\delta I + %
    R_0 \left( 1 + \beta_I \right) \delta I - %
    \frac{G \mathcal{L}_I}{I_0}\delta T
\end{gather*}
Here $\delta V$ is a change in $V_{th}$, e.g., sourced by a $\delta V_b = \frac{R_b\p + R_c}{R_{sh}}\delta V$ to the biasing voltage.
\end{namebox}

\begin{namebox}{Lineared TES matrix equation}{tes_linear}
Combining \myref{tes_thm_linear} and \myref{tes_ele_linear} into a matrix equation yields:
\begin{align*}
\frac{d}{dt}\left( \begin{array}{c} \delta I \\ \delta T \end{array} \right) &= -\underbrace{\left( \begin{array}{cc} \frac{R_{th}}{L} + \frac{1 + \beta_{I,0}}{L/R} & \frac{G\mathcal{L}_I}{LI_0} \\ -\frac{I_0 R_0}{C}\left( 2 + \beta_{I} \right) & \frac{1 - \mathcal{L}_I}{C/G} \end{array} \right)}_{\mathbb{M}} \left( \begin{array}{c} \delta I \\ \delta T \end{array} \right) + \left( \begin{array}{c} \frac{\delta V}{L} \\ \frac{\delta Q}{C} \end{array} \right)
\end{align*}
In Fourier space where time dependent quantities are expressed as $X(t) = \int X(\omega) e^{i\omega t} dt$, this matrix equation becomes:
\begin{align*}
i\omega\left( \begin{array}{c} \delta I \\ \delta T \end{array} \right)%
&= -\left( \begin{array}{cc} \frac{1}{\tau_{ele}} & \frac{G\mathcal{L}_I}{LI_0} \\ -\frac{I_0 R_0}{C}\left( 2 + \beta_{I} \right) & \frac{1}{\tau_{thm}} \end{array} \right) \left( \begin{array}{c} \delta I \\ \delta T \end{array} \right) + \left( \begin{array}{c} \frac{\delta V}{L} \\ \frac{\delta Q}{C} \end{array} \right) \\
&\text{where } \tau_{ele} \equiv \frac{L/R}{1 + \beta_{I} + R_{th}/R_0} %
\sps \tau_{thm} \equiv \frac{C/G}{1 - \mathcal{L}_I}
\end{align*}
For typical values for loop gain, $\mathcal{L}_I \sim 20$, $\tau_{thm}$ is negative, but $\tau_{thm}$ is only a part of the final time constants I compute (\red{refer to box}) which can be positive with high $\mathcal{L}_I$.
%
To shape this into a response matrix, I invert
$(i\omega\mathbb{I} + \mathbb{M})$:
\begin{gather*}
\left(\begin{array}{c} \delta I \\ \delta T \end{array}\right) = \mathbb{S}\left(\begin{array}{c} \delta V / L \\ \delta Q / C \end{array}\right)
\end{gather*} Where:
\begin{align*}
\mathbb{S} &= \frac{1}{W} \left( \begin{array}{cc} i\omega + \frac{1}{\tau_{ele}} & \frac{G\mathcal{L}_I}{LI_0} \\ -\frac{I_0 R_0}{C}\left( 2 + \beta_{I} \right) & i\omega + \frac{1}{\tau_{thm}} \end{array} \right) \\
W &\equiv \left( i\omega + \frac{1}{\tau_{ele}} \right)\left( i\omega + \frac{1}{\tau_{thm}} \right) + \frac{\mathcal{L}_I\left( 2 + \beta_{I} \right)}{(LC)/(GR_0)}
\end{align*}
\end{namebox}

The final matrix equation in \myref{tes_linear} gives the small-signal response in $I$ and $T$ from variations in $\delta V$ or $\delta Q$.
%
In the next few sections, this matrix relationship will be used to \red{list}.

\red{Emphasize the treatment of TES states as (T, I; Tb, Vb) equilibria}

\red{Describe the python sims a bit, but put an API in the appendix}
\red{(link to git in both locations)}

\red{Describe how different experiments change TESs for their particular purposes.}

\section{TES behavior --- heat impulses and bias steps}\label{tes_model1}

\subsection*{Heat impulse}

From the matrix equation in \myref{tes_linear}, we can examine how the TES reacts to a deposition of heat by a cosmic ray.
%
The TES is ignorant to the source of the heat deposition, so the resultant behavior is identical to a spike in optical loading which might model how the TES reacts to a flash of light.
%
We would prefer the TES stays on transition and does not oscillate during this behavior, so we are looking for \newterm{stable} and \newterm{overdamped} configurations.



These examinations rely on the homogenous solutions, shown in \myref{tes_homogeneous}.

\begin{namebox}{Homogeneous solutions}{tes_homogeneous}
Solving for $\delta I$ and $\delta T$ in the case where $\delta V =  \delta Q = 0$:
\begin{gather*}
\frac{d}{dt} \left( \begin{array}{c} \delta I \\ \delta T \end{array}
\right) = -\mathbb{M} \left( \begin{array}{c} \delta I \\ \delta T %
\end{array} \right) \Rightarrow %
\left( \begin{array}{c} \delta I \\ \delta T \end{array} \right) %
= A_+ \svec{v}_+ e^{-\lambda_+ t} + A_- \svec{v}_- e^{-\lambda_- t}
\end{gather*}
Solving for these eigenvectors I get:
\begin{gather*}
\svec{v}_{\pm} = \left( \begin{array}{c} \left(\tau_{thm}^{-1} - %
\lambda_{\pm}\right)\frac{C}{\left(2 + \beta_I\right)I_0 R_0} \\ 1 %
\end{array} \right) \\
\lambda_{\pm} = \frac{1}{2\tau_{ele}} + \frac{1}{2\tau_{thm}} \pm %
\sqrt{\left( \frac{1}{2\tau_{ele}} - \frac{1}{2\tau_{thm}} \right)^2 - %
\frac{\mathcal{L}_I(2 + \beta_{I,0})}{(LC)/(GR_0)}}
\end{gather*}
The inverse of these eigenvectors are the physical time constants, $\tau_\pm = \lambda_\pm^{-1}$.
%
For simplicity I have not normalized the eigenvectors.
\end{namebox}

In \myref{tes_homogenous} we have the time constants, which allows us to easily model how the current and temperature settles when they start out of equilibrium.
%
If both time constants are negative, then the detector is unstable and a small deviation from equilibrium leads to the TES falling off transition.
%
If a time constant is complex, the response has some oscillation to it.
%
We can set criteria on $L$ given $C, G$ and $R_0$ to avoid these undesirable conditions, but because I often vary many of these parameters concurrently, I check that each configuration has real and positive time constants.
%
A description of the cirtical inductances between which the detectors are stable is given in \red{ref% to I+H}.

It is important to note that even if the time constants at the detector's normal operating state, $I_0, T_0$, are real and positive, a nearby state with slightly different $R, G, C$ may not be stable and overdamped.
%
Therefore, any criteria dervived from keeping $\tau_\pm$ real and positive tends to over-estimate how reliable detectors are in practice.
%
It is necessary to check the neighborhood of usual operating states for reliability.
%
One observation from \myref{tes_homogeneous}: Because the two time constants differ by just a discriminant, they become equal in magnitude when underdamped, differing just by the sign of their imaginary components.
%
As the parameters of an overdamped detector are varied to approach the underdamped parameter space, the time constants converge to the same value.
%
If $\tau_+$ is much smaller than $\tau_-$, small deviations of the state will not cause the detector to become underdamped \red{quantify what ``small'' means}.



I solve for the response functions of stable and overdamped detectors in the case where a short fluctuation in $P_{opt}$ can be treated as an influx of excess heat, $\delta Q$, over a very short period of time, $\Delta t \ll \tau_+ < \tau_-$.
%
Or equivalently, consider the $\delta Q$ to be deposited by a particle interacting with the island in an even shorter period.
%
In either case, we treat the island's internal thermal conductivity to be high enough for the island temperature to quickly reach $T_0 + \Delta T$ and then we can solve for $T(t)$ and $I(t)$, shown in \myref{tes_response}.

\begin{namebox}{TES impulse response function}{tes_response}
Given an impulse which sets the starting state $\delta I(t=0)=0$, $\delta T(t=0) = \Delta T$, the evolution follows \myref{tes_homogeneous} as:
\begin{align*}
\delta I &= \left(\frac{\tau_{thm}}{\tau_+} - 1\right)\left(\frac{\tau_{thm}}{\tau_-} - 1\right)\left(\frac{1 - \mathcal{L}_I}{2 + \beta_{I}}\right) \left(\frac{G\Delta T}{I_0 R_0 \tau_{thm}}\right) \frac{e^{-t/\tau_+} - e^{-t/\tau_-}}{1/\tau_+ - 1/\tau_-} \\
\delta T &= \left( \left(\frac{1}{\tau_{thm}} - \frac{1}{\tau_+}\right)e^{-t/\tau_-} + \left(\frac{1}{\tau_{thm}} - \frac{1}{\tau_-}\right)e^{-t/\tau_+} \right) \frac{\Delta T}{1/\tau_+ - 1/\tau_-}
\end{align*}
\end{namebox}

These response functions are shown in \red{here}.
%
\red{make a plot of the response function (incl. saturation).}
%
Simulations of the response function in \red{myref} were generated with parameter values similar to SPIDER 1's $150 GHz$ TESs.
%
From examining $\delta I$, we can see that electro-thermal-feedback drives $\delta I$ negative, such that $0 < I < I_0$, until the excess temperature is dissipated and $\delta I$ can rise back to zero.
%
The fast downwards response is dominated by $\tau_+$, and the slower (but still fast!) recovery is dominated by $\tau_-$ \red{(give numbers)}.



This treatment fails for non-small signals.
%
In \red{point to saturation} we can see the heat deposition was sufficient to drive the TES off-transition, disabling the electro-thermal-feedback.
%
The film cools very slowly --- with a time constant closer to $C/G$ --- until the resistance dips below the normal resistance again and the effective self-cooling resumes.

\subsection*{Bias steps}

We can also examine how we can intentionally add fluctuations in the
biasing voltage and compute how the responding changes to the current
tells us the state of the TES. This is important because there is no
easy way to find $T$ or $R$, during regular operations,
so we must create methods to find $R$ with minimal interruption
to data-taking.

How does the TES current respond to fluctuations of
the biasing voltage? We call adding a small square wave to $V_b$
\newterm{bias steps} and they are a useful tool to ensure the TESs are
on-transition. The equations describing the response to such bias steps
are long and opaque but \red{show sims, real data, possibly my sims too}.
The period used for bias steps is much greater than the TES time
constants \red{(again, refer to plot of bias steps)}, so it is
sufficient to examine the $t\rightarrow \infty$ behavior of the TES
after a change in voltage bias as the bias step analysis compares the
current before and far into a step. The $t \rightarrow \infty$ behavior
is in \myref{tes_bias}.

\begin{namebox}{TES small change of bias}{tes_bias}
Starting from state $(I_0, T_0)$ at voltage $V_{b,0}$, changing the
voltage bias to $V_{b,0} + \delta V$ has the following effects at
$t \rightarrow \infty$:
\begin{align*}
\delta I &= \frac{\delta V}{R_0}\left( 1 + \frac{\mathcal{L}_I} %
{1 - \mathcal{L}_I}\left( 2 + \beta_I \right) + \beta_I \right)^{-1} \\
\delta T &= \frac{I_0 R_0}{G}\frac{2+\beta_I}{1 - \mathcal{L}_I} \delta I \\
\delta R &= \frac{R_0}{I_0}\left( \frac{\mathcal{L}_I} %
{1 - \mathcal{L}_I}\left( 2 + \beta_I \right) \right) \delta I
\end{align*}
The dynamic resistance of the TES is defined as $R_{dyn} \equiv %
\frac{\delta V}{\delta I}$ which approximates $R_0$ but for a factor:
\begin{align*}
R_{dyn} &= f_{bs} \cdot R_0 \text{ where } f_{bs} \equiv \left( 1 +  %
\frac{\mathcal{L}_I}{1 - \mathcal{L}_I} \left( 2 + \beta_I \right) + %
\beta_I \right)^{-1}
\end{align*}
\end{namebox}

The dynamic resistance is defined in \myref{tes_bias}.
On transition the loop gain is typically large enough for $f < 0$.
For example, for plausible operating values like $\mathcal{L}_I = 20$
and $\beta_I = 0.1$, $f \approx -90\%$. When the TES is off transition
and the loop gain drops to below unity, $f > 0$, thus an indicator the
TES is either normal or fully superconducting.

\red{sim of R vs R_dyn across transition}

\section{TES behavior --- loadcurves}\label{tes_model2}

\newterm{Loadcurves} are a useful diagnostic of detectors, capable of
measuring the function $R(V_{b})$ in normal operation conditions.
However, they are much more intrusive than bias steps in that they
cannot be inserted into data-taking and take a few minutes. The
procedure of a loadcurve follows these steps:
\begin{itemize}
\item Ensure the detector in question is in the normal state. This
is accomplished by either applying a \newterm{zap} (a pulse of heat to
the wafer), or if sufficient, applying the maximum $V_b$ available to
the detector ($2.5 V$).
\item Reduce the bias while keeping the detector normal, but allowing
the detector to cool. Often we zap the detectors while applying $2.5V$
of $V_b$ to a detector column, then reduce the applied bias to $\sim %
0.3V$. The science transitions for SPIDER 1 detectors are typically
found around $\sim 0.1 V$.
\item Once the FPU temperature has settled, take steps of $\sim 0.2 mV$
in bias from the high start point down to zero applied bias. Between
each step it is important to wait a few seconds to take the data point
to allow the island temperature, $T$, to equilibriate. Larger steps
require longer wait times, though consequently fewer steps are required
to take a full loadcurve.
\end{itemize}

Loadcurves are saved in data as a table of applied bias and resultant
TES signal. However, since the readout applies an offset to the data, we
fit a line to the normal portion of the TES current vs applied bias
curve and subtract away the y-intercept, in so doing assuming that the
detector has zero resistance when the science TES is fully super-
conduction and no parasitic resistances spoil this assumption. \red{How
do we know if this is true?} If this is true, then in the appropriate
units, the slope of the super-conducting portion of the loadcurve should
be unity.

Once the normal portion of the loadcurve is fit, the y-intercept is
removed and the slope is saved as the normal resistance of the science
transition, we can transform this \newterm{IV} representation of the
loadcurve into our desired \newterm{RV} curve.

Another form, the \newterm{PV} curve, highlights the saturation plateau
of the TES, which if taken in different conditions, can be used to
constrain the relative loadings \red{explain in more detail with example}.
\red{show examples of sim and real loadcurves and their representations}.
\red{comment on the effects of loading and $T_b$}

From a loadcurve we can derive the expected bias step derived dynamic
resistance, $R_{dyn}$, shown in \red{box for
computing bit amplitude to resistance, should followup TES small change
of bias} at a given bias.
Pairing this with the traditional RV curve, I can make a plot of the
factor, $f_{bs}(R_{dyn})$, described in \myref{tes_bias}.
This can help convert $R_{dyn}$ to the actual TES resistance.
While changes in $T_b$ and $P_{opt}$ will change the resistance at
a given bias, \red{Show this $f_{bs}$ is more robust}.

\subsection*{Noise model}

\section{Readout}\label{readout}

Describe TDM

Give a reference for FDM and MuMux

\subsection*{SQUID amplifiers}

Basically describe a theoretical tuning process.

Describe the readout noise model
(white noise + pink noise + roll-off)
Find a data-set with these features?


\subsection*{Multiplexing}

Describe the PID

Describe row-switching, the transients

Describe aliasing

Describe some nuances of bit math, filters, sampling.

\subsection*{Flux slips}

Show how an MCE can have a flux slip and some plots.

\subsection*{Total noise model}

A pretty plot?


