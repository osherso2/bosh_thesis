\chapter*{Chapter 3\\TESs and MCEs}
\addcontentsline{toc}{chapter}{Chapter 3: TESs and MCEs}
\def \chapterlabel {3}
\label{dets}
\setcounter{eqnum}{1}\setcounter{fignum}{1}



\begin{introfig}{spider_tile_glam.jpg}{dets:intro_pic}
%
A \spiderI wafer.
%
Photo taken by Marc Runyan.
%
\end{introfig}
\clearpage



This chapter provides background for Transition Edge Sensor (TES) based detectors.
%
All of the work in this thesis features discussion of TES behavior.
%
In addition to background for the next chapters, I wrote an extensive set of codes which implement the equations in this chapter.
%
This code-base allowed me to simulate and test the hypotheses in the following chapters.



In this chapter, I first describe the structures of a TES in section \ref{dets:phys_desc}.
%
The following section, \ref{dets:phys_desc}, details the equations I use to model TESs.
%
Section \ref{dets:tes_behave} describes the most relevant behaviors and diagnostics used in this work.
%
The final section, \ref{dets:simulations}, describes the code and links to its GitLab.



\section{Physical descriptions of TESs}\label{dets:phys_desc}



As previously discussed, modern CMB instruments use large arrays of TES bolometers that are lithographically fabricated onto silicon wafers.
%
A close up of such a bolometer, one of \spider's, is shown in \myref{dets:isl}.
%
A few features are immediately visible:
%
\begin{itemize}
%
\item[] The detector is comprised of a \newterm{island} of silicon nitride suspended over an etched-out canyon by zigzagged \newterm{legs}, allowing the substrate on which the TESs are patterned to have a significant temperature difference from the bulk of the wafer.
%
\item[] \spiderI has two TESs per island wired in series, one made out of titanium with a low critical temperature ($500 mK$), and one made out of aluminum with a high critical temperature ($1.3 K$).
%
\item[] The microstrips patterned on the right leg brings the biasing voltage onto the island.
%
\item[] The structures seen off-island are slot-antennas networked together by microstrips.
%
Power absorbed by one set of antennas flows to the resistive gold meander on the island by the waveguides patterned onto the left leg.
%
\end{itemize}



\begin{boxfig}{A \spiderI TES up close}{TES_island_magnified.jpg}{dets:isl}
%
A magnified picture of one of \spiderI's TES detectors, the same image as \myref{spider:wafers}B but zoomed in and with labeling focused on island components.
%
Indicated with $(1)$ is the titanium TES.
%
Indicated with $(2)$ is the aluminum TES.
%
Indicated with $(3)$ is the resistive gold meander.
%
\end{boxfig}



When \spider is deployed high in the atmosphere, the focal planes are illuminated by the dim cosmic light, faint atmosphere, and the small amount of internal loading.
%
The total flight loading is sufficiently low for the TES islands to cool to the science TES's critical temperature.
%
In these conditions, the aluminum TES is just a superconductor and can be ignored when considering detector dynamics.
%
However, it can be difficult to reproduce the dark environment of flight in the lab and consequentially difficult to keep the titanium TESs on-transition in testing.
%
This is why we also have the aluminum TES in series.
%
Because it shares the same island as the titanium TES (also known as the \newterm{science TES}), we can use the aluminum TES to characterize properties of the islands, as well as the optical chain which is agnostic to which TESs are on-transition.



By interlacing slot-antenna arrays with orthogonal polarization sensitivities onto the same space, a pair of detectors have nearly the same beam extending between the sky and the focal plane, but split into two polarizations.
%
We call these detector pairs \newterm{A/B pairs}.
%
We can define polarization axes such that one detector measures the $Q$ Stokes parameter and the other measures the $U$, though by rotation of the gondola and more significantly, rotation of the half-wave plate, each detector has sensitivity to both components over the flight.
%
This helps check for polarized systematics which may appear from the gain estimate being poorer for one detector than the other.



\section{Thermal and electrical model for the TES}\label{dets:sub_models}



In this section I describe the thermal and electrical balancing equations that keep the TES bolometers functioning.
%
I describe the choices I made to implement these balancing forces.
%
I call these particular choices \newterm{submodels}.
%
When putting everything together, I show the small-signal linear model.
%
For a pedagogical review of TESs, see \cite{chapter:Irwin_Hilton:TES:2005}, though I supplement with \cite{thesis:Rahlin:2016}.



\subsection*{Thermal model}



The thermal model we use to understand TES behavior is diagrammed in \myref{dets:thm_dia}.
%
The bulk of the wafer is at a base temperature, \newterm{$T_b$}, and has a large enough heat capacity and internal thermal conductivity so that $T_b$ varies slowly relative to TES time constants, and is treated as uniform across the wafer.



\begin{boxfig}{Thermal model}{tes_thm_model.pdf}{dets:thm_dia}
%
The thermal model for a TES: The island and all of its constituents are treated as isothermal with a heat capacity, $C(T)$.
%
The only path for heat to leave the island is through the combined leg thermal conductivity, $G(T, T_b)$.
%
\end{boxfig}



The combined leg thermal conductivity, $G$, is low enough for the island temperature, $T$, to reach nearly the TES critical temperature, $T_c > T_b$, but high enough for excess heat to flow out of the island\footnote{\spiderI island legs are zigzagged to reduce thermal conductivity between the island and the wafer, though this is a unique design.
%
Most other instruments have simpler leg geometries.}.
%
Power flows into the island from two sources: the \newterm{optical loading} from the antennas, and from Joule heating of the TES's own bias current.
%
Power is removed by heat flow along the legs.
%
The balance of these contributions leads to the equation in \myref{dets:thm_eq}:



\begin{namebox}{Thermal equation}{dets:thm_eq}
%
Temperature of the island changes with time in accordance with:
%
\begin{align*}
%
C \frac{dT}{dt} &= P_{opt} + I^2 R - P_{legs}
%
\end{align*}
%
Where $P_{opt}$ is the optical loading and $P_{legs}$ is the power that dissipates through the legs, $I$ is the current flowing through the TES which has resistance $R$.
%
\end{namebox}


The submodel for the thermal conductivity is a simple power-law in $G$, given in \myref{dets:thm_sub}:

\begin{namebox}{Thermal submodel}{dets:thm_sub}
%
Over modest ranges of temperature, it is reasonable to model leg thermal conductivity as a power law:
%
\begin{align*}
%
P_{legs} &= \int_{T_b}^{T} G(\tilde{T}) d\tilde{T} \sps
%
G(T) = G_x\left(\frac{T}{T_x}\right)^{\beta_G} \\
%
P_{legs} &= \frac{G_x}{T_x^{\beta_G}\left( \beta_G + 1\right)}\left( T^{\beta_G + 1} - T_b^{\beta_G + 1}\right) 
%
\end{align*}
%
Where $T_x = 450 mK$ is a typical reference temperature choice, so that $G_x = G(450 mK)$.
%
\end{namebox}



\subsection*{Electrical model}



The TES electrical model is shown in \myref{dets:ele_dia}.
%
The inductor, shunt resistor, and biasing components are off-wafer.
%
To analyze the electrical behavior, we can reduce the circuit to its Th\'evenin equivalent which is shown in \myref{dets:ele_thev}.
%
Using this simplified circuit, the balancing equation is in \myref{dets:ele_eq}:



\begin{boxfig}{Electrical model}{tes_ele_model.pdf}{dets:ele_dia}
%
A voltage source, an MCE bias digital to analogue converter (bDAC) supplies a voltage, $V_b$, behind a large bias resistor of resistance $R_b$, such that the parallel detector circuit is current biased by $I_b \approx V_b/R_b$.
%
The resistance of the TESs on the island is $R$.
%
    $R_{sh}$ is the shunt resistance and $L$ is the inductance that couples the TES current to a superconducting-quantum-interference-device (SQUID) (not shown here).
%
The dotted segment of the line indicates that there can be many of these detector circuits biased by a common $I_b$.
%
\end{boxfig}




\begin{boxfig}{Electrical Th\'evenin equivalent}{tes_ele_thev_model.pdf}{dets:ele_thev}
%
The Th\'evenin equivalent circuit for \myref{dets:ele_dia}.
%
These new variables are defined as:
%
\begin{gather*}
%
V_{th} \equiv \frac{V_b R_{sh}}{R_b + n R_c} \sps %
%
R_{th} \equiv R_{sh}, \\
%
\text{where } R_c(\omega) \equiv \frac{R_{sh}\left( R(\omega) + \omega L \right)}{R_{sh} + R(\omega) + \omega L} \approx R_{sh}
%
\end{gather*}
%
Where I approximate that all $n$ detectors biased by $I_b$ have identical $R$, $R_{sh}$, $L$.
%
Because $R_b$ is chosen to be much larger than $R_c$, $R_c$ can be ignored in $V_{th}$.
%
\end{boxfig}



\begin{namebox}{Electrical balance equation}{dets:ele_eq}
%
\begin{gather*}
%
V_{th} = \left(R_{th} + R\right) I + L \frac{dI}{dt}
%
\end{gather*}
%
\end{namebox}



The \newterm{state of the TES} is a point in $(I, T)$ space.
%
These, along with $R$, should be treated as the dependent coordinates to the independent coordinates, $V_b$, wafer temperature, $T_b$, and loading, $P_{opt}$.
%
The rest of the variables are parameters set at fabrication, such as $R_n$, the normal resistance, which is nominally $32 m\Omega$.



\begin{namebox}{Electrical submodels}{dets:ele_sub}
%
To model the resistance we use a simplified model used in \cite{thesis:Burney:2006}:
%
\begin{gather*}
%
R(I, T) = \frac{R_n}{2}\left( 1 + tanh\left( \frac{T - T_c\left(1 - %
%
\left(I/I_c\right)^{2/3}\right)}{T_c / \alpha_I^0} \right) \right)
%
\end{gather*}
%
In this submodel the normal resistance, $R_n$, critical temperature, $T_c$, and critical current, $I_c$, explicit.
%
The parameter $\alpha_I^0$ is a measure of the slope of the transition as a function of temperature when $I=0$.
%
More detail is provided in \myref{dets:alphabeta}.
%
\end{namebox}



For any transition shape, the \newterm{sharpness parameters} $\alpha_I$ and $\beta_I$ can be computed, defined in \myref{dets:alphabeta}:



\begin{namebox}{Sharpness parameters}{dets:alphabeta}
%
The two sharpness parameters which characterize the transition's sensitivity to temperature and current are $\alpha_I$ and $\beta_I$, respectively, and are defined as the dimensionless quantities:
%
\begin{align*}
%
\alpha_I &\equiv \frac{T}{R}\frac{dR}{dT}
%
= 2\alpha_I^0\left(1 - \frac{1}{R_n}\right)\left( \frac{T}{T_c} \right)\\
%
\beta_I &\equiv \frac{I}{R}\frac{dR}{dI}
%
= \frac{4}{3}\alpha_I^0\left(1 - \frac{R}{R_n}\right)\left(\frac{I}{I_c}\right)^{2/3}
%
\end{align*}
%
The second equalities are only valid for the tanh model.
%
In this model, $\beta_I^0 = \frac{2}{3}\alpha_I^0$, but in regular operation, $\beta_I \ll \beta_I^0$.
%
\end{namebox}



\subsection*{Putting the model together}



When the optical loading fluctuates by a small amount, $\delta P_{opt}$, the island temperature also fluctuates by $\delta T$, which causes a significant $\delta R$.
%
Because the TES is voltage-biased, the resultant current fluctuation, $\delta I$, has the opposite sign of $\delta R$ and $\delta P_{opt}$.
%
This creates a negative feedback: The Joule power, $\delta P_J$, fluctuates in the opposite direction as $\delta P_{opt}$.
%
This negative feedback is what keeps TES detectors on-transition, termed \newterm{electro-thermal-feedback} (ETF).
%
To better understand this process, I linearize the coupled differential equations in \myref{dets:tes_thm_linear} and \myref{dets:tes_ele_linear}:



\begin{namebox}{Linearized TES thermal behavior}{dets:tes_thm_linear}
%
Consider small changes in $I$ and $T$ away from the point $(I_0, T_0)$.
%
Expanding $R$ to first order we get:
%
\begin{align*}
%
R &= R_0\left(1 + \alpha_I\frac{\delta T}{T_0} + %
%
\beta_I \frac{\delta I}{I_0} \right) \\
%
P_J &= P_{J,0} \left(1 + \alpha_I \frac{\delta T}{T_0} + %
%
\beta_I \frac{\delta I}{I_0} + 2I_0 R_0 \delta I \right) \\
%
P_{legs} &= P_{legs, 0} \left( 1 + G_0 \delta T \right)
%
\end{align*}
%
Suppose this change in $I, T$ is accompanied by an impulse of heat to the island, $\delta Q$.
%
The linear thermal balance equation is then:
%
\begin{gather*}
%
C\frac{d\delta T}{dt} = \delta Q - G(1 - \mathcal{L}_I) \delta T %
%
+ I_0 R_0 \left(2 + \beta_I \right) \delta I %
%
\text{ where } \mathcal{L}_I \equiv \frac{P_{J, 0} \alpha_I}{G T_0}
%
\end{gather*}
%
The quantity $\mathcal{L}_I$ is called the \newterm{loop gain}.
%
\end{namebox}



\begin{namebox}{Linearized TES electrical behavior}{dets:tes_ele_linear}
%
Given the same small change in $I, T$ as in \myref{dets:tes_thm_linear}, but including a possible $\delta V$, the electrical balance becomes:
%
\begin{gather*}
%
L \frac{d\delta I}{dt} = \delta V - R_{th}\delta I + %
%
R_0 \left( 1 + \beta_I \right) \delta I - %
%
\frac{G \mathcal{L}_I}{I_0}\delta T
%
\end{gather*}
%
Here $\delta V$ is a change in $V_{th}$, \ie, sourced by a $\delta V_b = \frac{R_b\p + R_c}{R_{sh}}\delta V$ to the biasing voltage.
%
\end{namebox}



Combining these expanded equations into a single matrix expression allows us to solve for $(\delta I, \delta T)$ when the system is perturbed by any small $(\delta V, \delta Q)$ combination:



\begin{namebox}{Lineared TES matrix equation}{dets:tes_linear}
%
Combining \myref{dets:tes_thm_linear} and \myref{dets:tes_ele_linear} into a matrix equation yields:
%
\begin{align*}
%
\frac{d}{dt}\left( \begin{array}{c} \delta I \\ \delta T \end{array} \right) &= %
%
-\underbrace{\left( \begin{array}{cc} \frac{R_{th}}{L} + \frac{1 + \beta_{I,0}}{L/R} & \frac{G\mathcal{L}_I}{LI_0} \\ -\frac{I_0 R_0}{C}\left( 2 + \beta_{I} \right) & \frac{1 - \mathcal{L}_I}{C/G} \end{array} \right)}_{\mathbb{M}}%
%
\left( \begin{array}{c} \delta I \\ \delta T \end{array} \right) + \left( \begin{array}{c} \frac{\delta V}{L} \\ \frac{\delta Q}{C} \end{array} \right)
%
\end{align*}
%
Expressing time dependent quantities in Fourier space ($X(t) = \int X(\omega) e^{i\omega t} dt$) yields:
%
\begin{align*}
%
i\omega\left( \begin{array}{c} \delta I \\ \delta T \end{array} \right)%
%
&= -\left( \begin{array}{cc} \frac{1}{\tau_{ele}} & \frac{G\mathcal{L}_I}{LI_0} \\ -\frac{I_0 R_0}{C}\left( 2 + \beta_{I} \right) & \frac{1}{\tau_{thm}} \end{array} \right) \left( \begin{array}{c} \delta I \\ \delta T \end{array} \right) + \left( \begin{array}{c} \frac{\delta V}{L} \\ \frac{\delta Q}{C} \end{array} \right) \\
%
&\text{where } \tau_{ele} \equiv \frac{L/R}{1 + \beta_{I} + R_{th}/R_0} %
%
\sps \tau_{thm} \equiv \frac{C/G}{1 - \mathcal{L}_I}
%
\end{align*}
%
For typical loop gain values of $\mathcal{L}_I \gg 1$, $\tau_{thm}$ is negative, but $\tau_{thm}$ is only a part of the final time constants, shown in \myref{dets:tes_homogeneous}, which can be positive with high $\mathcal{L}_I$.
%
To shape this into a response matrix, I invert
%
$(i\omega\mathbb{I} + \mathbb{M})$:
%
\begin{gather*}
%
\left(\begin{array}{c} \delta I \\ \delta T \end{array}\right) = \mathbb{S}\left(\begin{array}{c} \delta V / L \\ \delta Q / C \end{array}\right)
%
\end{gather*} Where:
%
\begin{align*}
%
\mathbb{S} &= \frac{1}{W} \left( \begin{array}{cc} i\omega + \frac{1}{\tau_{ele}} & \frac{G\mathcal{L}_I}{LI_0} \\ -\frac{I_0 R_0}{C}\left( 2 + \beta_{I} \right) & i\omega + \frac{1}{\tau_{thm}} \end{array} \right) \\
%
W &\equiv \left( i\omega + \frac{1}{\tau_{ele}} \right)\left( i\omega + \frac{1}{\tau_{thm}} \right) + \frac{\mathcal{L}_I\left( 2 + \beta_{I} \right)}{(LC)/(GR_0)}
%
\end{align*}
%
\end{namebox}



\section{TES behavior}\label{dets:tes_behave}



From the matrix equation in \myref{dets:tes_linear}, we can examine how the TES reacts to an energy impulse, \eg deposition of heat by a cosmic ray.
%
We would prefer the TES stays on transition and does not oscillate during this behavior, so we are looking for \newterm{stable} and \newterm{overdamped} configurations.
%
To examine those traits, we look at the homogeneous solutions, shown in \myref{dets:tes_homogeneous}:



\begin{namebox}{Homogeneous solutions}{dets:tes_homogeneous}
%
Solving for $\delta I$ and $\delta T$ in the case where $\delta V =  \delta Q = 0$:
%
\begin{gather*}
%
\frac{d}{dt} \left( \begin{array}{c} \delta I \\ \delta T \end{array}
%
\right) = -\mathbb{M} \left( \begin{array}{c} \delta I \\ \delta T %
%
\end{array} \right) \Rightarrow %
%
\left( \begin{array}{c} \delta I \\ \delta T \end{array} \right) %
%
= A_+ \svec{v}_+ e^{-\lambda_+ t} + A_- \svec{v}_- e^{-\lambda_- t}
%
\end{gather*}
%
Solving for these eigenvectors I get:
%
\begin{gather*}
%
\svec{v}_{\pm} = \left( \begin{array}{c} \left(\tau_{thm}^{-1} - %
%
\lambda_{\pm}\right)\frac{C}{\left(2 + \beta_I\right)I_0 R_0} \\ 1 %
%
\end{array} \right) \\
%
\lambda_{\pm} = \frac{1}{2\tau_{ele}} + \frac{1}{2\tau_{thm}} \pm %
%
\sqrt{\left( \frac{1}{2\tau_{ele}} - \frac{1}{2\tau_{thm}} \right)^2 - %
%
\frac{\mathcal{L}_I(2 + \beta_{I,0})}{(LC)/(GR_0)}}
%
\end{gather*}
%
The inverse of these eigenvectors are the physical time constants, $\tau_\pm = \lambda_\pm^{-1}$.
%
For simplicity I have not normalized the eigenvectors.
%
\end{namebox}



In \myref{dets:tes_homogeneous} are the time constants, which determine how quickly the current and temperature can change or settle when pushed out of equilibrium.
%
If either time constant is negative, then the detector is unstable and a small deviation from equilibrium leads to the TES falling off transition.
%
If time constants are complex, the response has some oscillation to it.
%
We can set criteria on $L$ given $C, G$ and $R_0$ to avoid these undesirable conditions.
%
An equation of the critical inductances between which the detectors are stable is given in \cite{chapter:Irwin_Hilton:TES:2005}, and a descriptive plot is in \myref{dets:taus}.



\begin{boxfig}{Critical and threshold inductances}{taus.png}{dets:taus}
%
Time constant dependence on inductance, with all other parameters fixed.
%
This simulated detector is similar to the titanium transition in the 150s.
%
The two time constants are real, positive, and separated on the left where the detector is stable and overdamped (indicated by the green bar on top and ``s+o'').
%
At $L_{crit}^- < L < L_{crit}^+$ the two time constants converge and have an equal magnitude but opposite signed imaginary components (shown in dashed lines).
%
The detector is underdamped in this and region can be either stable or unstable, as indicated by the orange (``s+u'') and grey (``u+u'') bars, respectively.
%
The division between these two regions is $ L_{thresh}$.
%
Finally, there is a region of instability for $L > L_{crit}^+$.
%
\end{boxfig}



It is important to note that even if the time constants at the detector's normal operating state, $I_0, T_0$, are real and positive, a nearby state with slightly different $(I, T)$ --- therefore different $R, G, C$ --- may not be stable and overdamped.
%
Therefore, any criteria derived from keeping $\tau_\pm$ real and positive tends to over-estimate how stable detectors are in practice.
%
It is this necessary to check the neighborhood of usual operating states for reliability.



One observation from \myref{dets:tes_homogeneous}: Because the two time constants differ by just a discriminant, they become equal in magnitude when underdamped, differing just by the sign of their imaginary components.
%
As an overdamped detector approaches the underdamped parameter space, the time constants converge to the same value.
%
This means that if $\tau_+$ is much smaller than $\tau_-$, small deviations of the state will not cause the detector to become underdamped and the experimentalist can feel more secure in the detector's reliability.



\subsection*{Impulse response}



I solve for the response functions of stable and overdamped detectors in the case where a short fluctuation in $P_{opt}$ can be treated as an influx of excess heat, $\delta Q$, over a very short period of time, $\Delta t \ll \tau_+ < \tau_-$.
%
Or equivalently, consider the $\delta Q$ to be deposited by a particle interacting with the island in an instant.
%
In either case, we assume the island's internal thermal conductivity to be high enough for the island temperature to quickly equilibriate at some $T_0 + \Delta T$ and then we can solve for $T(t)$ and $I(t)$, shown in \myref{dets:tes_response}:



\begin{namebox}{TES impulse response function}{dets:tes_response}

Given an impulse which sets the starting state $\delta I(t=0)=0$, $\delta T(t=0) = \Delta T$, the evolution follows \myref{dets:tes_homogeneous} as:
%
\begin{align*}
%
\delta I &= \left(\frac{\tau_{thm}}{\tau_+} - 1\right)\left(\frac{\tau_{thm}}{\tau_-} - 1\right)\left(\frac{1 - \mathcal{L}_I}{2 + \beta_{I}}\right) \left(\frac{G\Delta T}{I_0 R_0 \tau_{thm}}\right) \frac{e^{-t/\tau_+} - e^{-t/\tau_-}}{1/\tau_+ - 1/\tau_-} \\
%
\delta T &= \left( \left(\frac{1}{\tau_{thm}} - \frac{1}{\tau_+}\right)e^{-t/\tau_-} + \left(\frac{1}{\tau_{thm}} - \frac{1}{\tau_-}\right)e^{-t/\tau_+} \right) \frac{\Delta T}{1/\tau_+ - 1/\tau_-}
%
\end{align*}
%
\end{namebox}



A simulation of the impulse response function, shown in \myref{dets:sims}A, was generated with parameter values similar to \spiderI's $150 GHz$ TESs.
%
From examining $\delta I$, we can see that electro-thermal-feedback initially drives $\delta I$ negative, such that $0 < I < I_0$, until the excess heat is dissipated and $\delta I$ can decay to zero.
%
The fast downwards response is dominated by $\tau_+ \approx 0.1 ms$ while the slower recovery is dominated by $\tau_- \approx 5ms$.



This small-signal treatment fails for large depositions.
%
For example, a $\gtrsim 10 keV$ heat deposition is sufficient to drive the TES off-transition, disabling the electro-thermal-feedback.
%
Because ETF is the mechanism which greatly speeds up the recovery time constant, without it the film cools very slowly --- with a time constant closer to $C/G$ --- until the effective self-cooling resumes.



\subsection*{Bias steps}



We can also examine the effect of intentionally adding small fluctuations in the biasing voltage.
%
These added square waves are called \newterm{bias steps} and they turn out to be a useful method of evaluating a TES's state with minimal interruption of normal data taking.
%
The equations describing the response to such bias steps are long and opaque but some example bias steps on simulated $150 GHz$ detectors are shown in \myref{dets:sims}B.



There is a ring to bias steps when the TES is on transition.
%
Additionally, the on-transition response is inverted relative to that of a TES in its normal state or superconducting state.
%
The amplitude of the step measures the \newterm{dynamic resistance}, $R_{dyn} \equiv dV/dI$, the computation of which is given in \myref{dets:tes_bias}:



\begin{namebox}{TES small change of bias}{dets:tes_bias}
%
Starting from state $(I_0, T_0)$ at voltage $V_{b,0}$, changing the voltage bias to $V_{b,0} + \delta V$ has the following effects at $t \rightarrow \infty$:
%
\begin{align*}
%
\delta I &= \frac{\delta V}{R_0}\left( 1 + \frac{\mathcal{L}_I} %
%
{1 - \mathcal{L}_I}\left( 2 + \beta_I \right) + \beta_I \right)^{-1} \\
%
\delta T &= \frac{I_0 R_0}{G}\frac{2+\beta_I}{1 - \mathcal{L}_I} \delta I \\
%
\delta R &= \frac{R_0}{I_0}\left( \frac{\mathcal{L}_I} %
%
{1 - \mathcal{L}_I}\left( 2 + \beta_I \right) \right) \delta I
%
\end{align*}
%
The dynamic resistance of the TES is defined as $R_{dyn} \equiv \delta V/\delta I$ which approximates $R_0$ but for a factor:
%
\begin{align*}
%
R_{dyn} &= f_{bs} \cdot R_0 \text{ where } f_{bs} \equiv \left( 1 +  %
%
\frac{\mathcal{L}_I}{1 - \mathcal{L}_I} \left( 2 + \beta_I \right) + %
%
\beta_I \right)^{-1}
%
\end{align*}
%
For \spiderI devices, $f_{dyn} \approx -0.9$.
%
\end{namebox}



With the knowledge that $f_{dyn} \approx -1$, we use $R_{dyn}$ measured by bias steps to approximate $R$.



\begin{boxpairfigs}{TES response functions}{sim_pulses.png}{sim_bs.png}{dets:sims}
%
\begin{itemize}
%
\item[\bf A] Simulations of instantaneous energy depositions to the TES island with the corresponding change in temperatures indicated in the legend.
%
\item[\bf B] Responses of a simulated TES to $\delta V = 5 mV$ square wave applioed to the TES bias done at multiple initial TES resistances.
%
The points show how the data would look at the \spiderI flight sample rate, the lines shows the same responses in more detail.
%
Note the on-transition detectors have an addition spike at each change in bias, but the low sample-rate data does not capture this.
%
\end{itemize}
%
\end{boxpairfigs}



\subsection*{Loadcurves}



\newterm{Loadcurves} are another useful diagnostic of detectors, capable of measuring the function $R(V_{b})$ and the TES power balance.
%
However, they are much more intrusive than bias steps in that they take several minutes and disrupt the TES's working state.
%
The procedure of a loadcurve follows these steps:
%
\begin{itemize}
%
\item Ensure that the TES in question is in the normal state.
%
This is accomplished by either applying the maximum $V_b$ available to the detector ($2.5 V$), or a pulse of heat to the wafer (called a \newterm{zap}) if the maximum bias is insufficient%
\footnote{As is often the case in \spiderI}.
%
\item Reduce the bias while keeping the detector normal, allowing the detector to cool.
%
We often reduce the applied bias to $\sim 0.3V$: the science transitions for \spiderI detectors are typically found around $\sim 0.1 V$.
%
\item Once the FPU temperature has settled, take steps of $\mathcal{O}\left(-1 mV\right)$ in bias from the high start point down to zero applied bias.
%
Between each step we typically wait hundred milliseconds to take the data point, so that the island temperature equilibriates at the new bias.
%
\end{itemize}



A simulation of a loadcurve is in \myref{dets:loadcurves}, subfigure A illustrates the current against applied bias representation that is closest to raw loadcurve data used in analysis. 
%
However, as the MCE records applied feedback, rather than TES current directly, some data corrections are often needed.
%
This feedback signal is only a proxy for the TES current and designed to accurately track small fluctuations about a set-point, not the wide range of values the TES current traverse during a loadcurve.



Because a loadcurve is a big signal, the feedback applied by the MCE can change by more than a SQUID flux quantum from the start of the loadcurve to the end.
%
The MCE has a limited range of feedback it can apply, so in taking advantage of the SQUID's periodicity, the MCE can shift the applied feedback by a flux quantum.
%
The MCE tries to hide these jumps by adding in the number of SQUID flux quanta it has registered to the data.
%
However, inaccurate SQUID flux quanta or cross-talk can leave small residuals in the data after the automatic stitching.
%
These little jumps are small, though occasionally require more manual stitching to correct the loadcurves.



After this fix, we need to remove a small offset and multiplicative factor.
%
We fit a line to the normal portion of the TES current vs applied bias curve and subtract away the $y$-intercept, in so doing assuming that the detector has zero resistance when the science TES is fully super-conducting.
%
The slope of this line is the normal resistance.
%
We also assume the super-conducting portion should have slope of one in the units of applied bias current vs. TES current.
%
Enforcing this slope requires multiplying the data by a factor very close to unity.
%
This last step can not always be automated because the MCE \newterm{loses lock} which best is solved by cropping the data to include the superconducting curve preceding the loss of lock. 
%
Once these steps are followed we have a fixed up bias voltage versus TES current.



From the \newterm{IV} (current against applied voltage) representation of the loadcurve, we can compute the \newterm{RV} curve  --- the resistance against applied voltage --- or the \newterm{PV} form --- Joule power against applied voltage.
%
The latter highlights the saturation plateau of the TES, which identifies the amount of power needed on the island to get the TES on transition (not counting the configuration's FPU temperature and optical loading).
%
By comparing the saturation power to one computed when the detectors face a black, $300 mK$ \newterm{blanking plate}, the optical loading of a configuration can be well measured%
\footnote{In a single run, a light channel can be compared against a dark TES detector.}. 



One example of such work is our attempts to find the source of excess loading in a 280 receiver.
%
By blanking off different parts of the cryostat and taking loadcurves, we search for which component in the telescope is adding optical load to the detectors.



\begin{boxfig}{Loadcurve simulation}{loadcurves.png}{dets:loadcurves}

A loadcurve is simulated with a virtual \spider 150 detector and by taking small steps in bias, waiting a little bit and recording the current, which is repeated until a large portion of the TES transition of interest is covered.
%
In these plots, time and $V_b$ are linearly related ($5 mV$ steps taken every $0.1s$) and because this figure is generated in simulation, the transients can be shown between the larger dots which indicate the limited data that would be recorded in practice.
%
The color gradient across each curve indicates the bias: yellow corresponds the high start bias of $V_b = 200 mV$, and dark purple is the terminus of $60 mV$.
%
\begin{itemize}
%
\item[\bf A] This is the loadcurve as it is recorded by the MCE. %
%
The linear and normal region is shaded green.
%
The superconducting region is shaded blue, and the transition is in the intermediate cyan.
%
\item[\bf B] The $(R(V_b(t)), t)$ plot, derivable from {\bf A}, explicitly shows the transition in resistance. 
%
\item[\bf C] The Joule power as the voltage steps down, derived from {\bf A}, shows the saturation plateau of the transition.
%
There is a slight tilt to this plateau due to non-zero $\beta_I$.
%
\item[\bf D] This is the $f_{dyn}$ plot that can related measured $R_{dyn}$ to $R$ and can also be derived from {\bf A}.
%
\item[\bf E] This is the path a loadcurve takes through $(I, T)$ space.
%
This is {\bf not} derivable from a loadcurve and would be hard to measure outside of a simulation.
%
We see that during the transition $T \lesssim T_c$.
%
\item[\bf F] Once the virtual detector has settled at each step of the simulated loadcurve, I add $0.01 pW$ of $P_{opt}$ to measure the resultant change in $P_{J}$ before returning $P_{opt}$ to its approximate in-flight value.
%
This gives a measure of power responsivity, which is consistently $\gtrsim 90\%$ during the transition and otherwise near zero.
%
This plot is also not derivable from a loadcurve.
%
Similar probing with a calibrated light source would be necessary to measure this in the lab.
%
\end{itemize}


\end{boxfig}



From a loadcurve we can also derive the expected dynamic resistance at each bias.
%
Pairing this with the traditional RV curve, I can make a plot of the factor, $f_{bs}(R_{dyn})$, described in \myref{dets:tes_bias}.
%
This can help convert $R_{dyn}$ computed from bias steps in later datasets to the actual TES resistance with higher accuracy than assuming $f_{dyn} \approx -1$.
%
Such a loadcurve-derived conversion graph is included in \myref{dets:loadcurves}.



\section{TES Simulations}\label{dets:simulations}



Many figures from this chapter were generated in simulation.
%
These were all created using object-oriented \python code I wrote, called \EST%
\footnote{EST: EST Simulates TESs}.
%
Unlike its predecessor codes, my modular code allows for multiple different TESs in series (\eg science and lab TESs simulated together) without linear approximation.
%
Full sigmoid functions for $R(I, T)$ are used with appropriately varying $\beta_I$ and $\alpha_I$ parameters at each TES state.



At the core of the simulation is the ability to compute the change in TES state in a small step in time, built to accurately model behavior even beyond the linear regime.
%
While these steps in time are sufficient for most uses, they can be impractical:
%
When changing the bias, a lot of computing time can be wasted to let the simulation find the new equilibrium TES state by taking millions of small steps.
%
Worse still, if the user wants to set the simulated detector to a spot on the transition like $R/R_n = 60\%$, a virtual loadcurve would need to be taken to find the right voltage to virtually apply.
%
To mitigate these wastes of time, I implemented solvers for these two particularly wasteful computations that do not rely on taking many small steps in time.
%
By minimizing the power balance, $P_{opt} + P_{Joule} - P_{legs}$, with different inputs, along with many heuristics I found in developing this, my code quickly finds biases and equilibrium values.



I used and further developed this code in many of my projects such that it works for both \spider detectors and detectors in design for another project: \cmbsIV describe in chapter \ref{cmbs4}.
%
The transition from one family of detectors to the next was smooth and I believe the code is robust to handle many such investigations. 
%
When looking into the MCE, I also implemented an MCE simulation add-on I found to be cool and useful.
%
When studying the noise from detectors, I created detector noise models described in the next chapter, which are integrated into the same code-base.
%
It can be found in our GitLab: \href{https://gitlab-beta.engr.illinois.edu/osherso2/EST}{gitlab-beta.engr.illinois.edu/osherso2/EST}.
%
An example script using the main functions of my code is provided in \ref{appn:code:EST}.
