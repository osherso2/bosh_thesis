\chapter*{Appendix A\\More Cosmology Equations}
\addcontentsline{toc}{chapter}{Appendix A: More Cosmology Equations}
\def \chapterlabel {A}
\label{appn:cosm}
\setcounter{eqnum}{1}\setcounter{fignum}{1}



\section{General cosmology derivations}



\begin{namebox}{Conformal time}{appn:cosm:conformal}
%
The amount of conformal time that passes in the interval $(t, t+\Delta t)$:
%
\begin{gather*}
%
\Delta \eta = \int_t^{t + \Delta t} \frac{d\tilde{t}}{a(\tilde{t})}d\tilde{t}
%
\end{gather*}
%
Derivatives in conformal time, $X\p$ can be related to derivatives in physical time, $\dot{X}$:
%
\begin{gather*}
%
X\p(\eta) \equiv \frac{d X}{d\eta} = \frac{dt}{d\eta}\frac{d}{dt}X\left(t(\eta)\right) = a(t) \dot{X}(t)
%
\end{gather*}
%
\end{namebox}



\begin{namebox}{Conformal Hubble parameter}{appn:cosm:conf_hub}
%
The \newterm{conformal Hubble parameter} is:
%
\begin{gather*}
%
\mathcal{H} \equiv \frac{a\p}{a} = \frac{a\dot{a}}{a} = \dot{a} = aH
%
\end{gather*}
%
\end{namebox}



\begin{namebox}{FLRW metrix to Ricci scalar}{appn:cosm:coeffs}
%
The FLRW metric (\myref{cosm:flrw}) in matrix notation is:
%
\begin{align*}                                                        
%
\mathbf{g} \cdot \vec{x} &=                                         
\left(                                                              
\begin{array}{cccc}                                               
1 & 0 & 0 & 0 \\                                                
0 & -\frac{a^2}{1 - kr^2} & 0 & 0 \\                            
0 & 0 & -a^2r^2 & 0 \\                                          
0 & 0 & 0 & -a^2r^2\sin^2(\theta)                               
\end{array}                                                       
\right)                                                           
\left( \begin{array}{c} t \\ r \\ \theta \\ \phi \end{array} \right)
\end{align*}                                                          
%
The non-zero derivatives of the metric are:                          
%
\begin{gather*}                                                       
%
\begin{array}{l}                                                    
\partial_t g_{rr} = -\frac{2a\dot{a}}{1 - kr^2} \\                
\partial_r g_{rr} = -\frac{2a^2 kr}{\left(1 - kr^2\right)^2}      
\end{array} \                                                       
\begin{array}{l}                                                    
\partial_t g_{\theta\theta} = -2a\dot{a}r^2 \\                    
\partial_r g_{\theta\theta} = -2a^2 r                             
\end{array} \                                                       
%
\begin{array}{l}                                                    
\partial_t g_{\phi\phi} = -2a\dot{a}r^2 \sin^2(\theta) \\         
\partial_r g_{\phi\phi} = -2a^2 r \sin^2(\theta) \\               
\partial_\theta g_{\phi\phi} = -2a^2 r^2 \sin(\theta) \cos(\theta)
\end{array}                                                         
\end{gather*}                                                         
%
The only surviving Christoffel symbols are:
%
\begin{align*}                                                        
%
\text{from} \ & \Gamma^\mu_{\ \alpha\beta} = \frac{g^{\mu\nu}}{2} \left( \partial_\alpha g_{\beta\nu} + \partial_\beta g_{\alpha\nu} - \partial_\nu g_{\alpha\beta} \right) \\
%
\Gamma^t_{\ ij} &= \frac{g^{t\nu}}{2}\left(\partial_j g_{i\nu} + \partial_i g_{j\nu} - \partial_\nu g_{ij} \right) = -\frac{1}{2}\partial_t g_{ij}\delta^i_{\ j} \\
%
\Gamma^i_{\ tj} &= \frac{g^{i\nu}}{2}\left(\partial_j g_{t\nu} + \partial_t g_{j\nu} - \partial_\nu g_{tj} \right) = \frac{g^{ij}}{2}\partial_t g_{ji}\delta^i_{\ j} \\
%
\Gamma^k_{\ ij} &= \frac{g^{k\nu}}{2}\left(\partial_j g_{i\nu} + \partial_i g_{j\nu} - \partial_\nu g_{ij} \right)
%
\end{align*}                                                          
%
The latin indices stand in for $\{ r, \theta, \phi \}$, explicitly:
%
\begin{gather*}                                                       
%
\Gamma^t_{\ rr} = \frac{a\dot{a}}{1 - kr^2} \ \ \ \ \Gamma^t_{\ \theta\theta} = a\dot{a}r^2 \ \ \ \ \Gamma^t_{\ \phi\phi} = a\dot{a}r^2\sin^2(\theta) \ \ \ \ \Gamma^r_{\ tr} = \Gamma^\theta_{\ t\theta} = \Gamma^\phi_{\ t\phi} = \frac{\dot{a}}{a} \\
%
\Gamma^r_{\ rr} = \frac{kr}{1 - kr^2} \ \ \ \ \Gamma^r_{\ \theta\theta} = -r\left( 1 - kr^2\right) \ \ \ \ \Gamma^r_{\ \phi\phi} = -r\left( 1 - kr^2\right)\sin^2(\theta) \\
%
\Gamma^\theta_{\ \phi\phi} = -\sin(\theta)\cos(\theta) \ \ \ \ \ \ \Gamma^\theta_{\ r\theta} = \Gamma^\phi_{\ r\phi} = \frac{1}{r} \ \ \ \ \ \ \Gamma^\phi_{\ \theta\phi} = \cot(\theta)
%
\end{gather*}                                                         
%
The Ricci tensor's nonzero components and trace are:
%
\begin{gather*}                                                       
%
\text{from} \ R_{\mu\nu} = \partial_\alpha \Gamma^\alpha_{\ \mu\nu} - \partial_\nu \Gamma^\alpha_{\ \mu\alpha} + \Gamma^\alpha_{\ \beta\alpha}\Gamma^\beta_{\ \mu\nu} + \Gamma^\alpha_{\ \beta\nu}\Gamma^\beta_{\ \mu\alpha}
%
\end{gather*}\vspace{-.75cm}                                          
%
\begin{align*}                                                        
%
&R_{tt} = -3\frac{\ddot{a}}{a} && R_{rr} = \frac{a\ddot{a} + 2\dot{a}^2 + 2k}{1 - kr^2} \\
%
&R_{\theta\theta} = r^2\left( a\ddot{a} + 2\dot{a}^2 + 2k\right) && R_{\phi\phi} = r^2\left( a\ddot{a} + 2\dot{a}^2 + 2k\right)\sin^2(\theta)
%
\end{align*}\vspace{-.75cm}  
%
\begin{gather*}                                                       
%
\mathcal{R} = g^{\mu\nu}R_{\mu\nu} = -6\left( \frac{\ddot{a}}{a} + \frac{\dot{a}^2}{a^2} + \frac{k}{a^2} \right)
%
\end{gather*} 
%
\end{namebox}



\begin{namebox}{A perfect fluid}{appn:cosm:fluid}
%
A perfect fluid is one where the entire fluid is moving with velocity $\vec{u}$ and has uniform energy density, $\rho$, and pressure, $P$.
%
Its stress-energy tensor is:
%
\begin{gather*}
%
T_{\alpha\beta} = (\rho + P)u_\alpha u_\beta - P g_{\alpha\beta}
%
\end{gather*}
%
For a fluid in our FLRW metric, $\vec{u} = (1, 0, 0, 0)$, and using an equation of state, $P = w\rho$:
%
\begin{gather*}
%
T_{\alpha\beta} = \rho \left((w + 1)\delta^t_\alpha \delta^t_\beta - wg_{\alpha\beta}\right)
%
\end{gather*}
%
Where $\delta^a_b$ is the Kronecker delta.
%
Note that if $w = -1$, this matches $\Lambda g_{\alpha\beta} / 8\pi$ where $\rho = \Lambda \ 8 \pi$.
%
\end{namebox}



\begin{namebox}{Deriving the Friedmann equations}{appn:cosm:friedmanns}
%
The first Friedmann equation is the $tt$ component of Einstein's equation (\myref{cosm:ee}).
%
Using the values derived in \myref{appn:cosm:coeffs} and a perfect fluid stress-energy tensor from \myref{appn:cosm:fluid}:
%
\begin{gather*}
%
G_{tt} = R_{tt} - \frac{1}{2}\mathcal{R} = 8\pi G T_{tt} \\
%
\Rightarrow 3\left(\frac{\dot{a}^2 + k}{a^2}\right) =  8 \pi G\rho
%
\end{gather*}
%
The second Friedmann equation is the trace, $g^{\mu\nu} G_{\mu\nu}$ of Einstein's equation:
%
\begin{align*}                                                        
%
&g^{\mu\nu}G_{\mu\nu} = g^{\mu\nu}\left( R_{\mu\nu} - \frac{1}{2}g_{\mu\nu}\mathcal{R} \right) = -\mathcal{R} = 6\left( \frac{\ddot{a}}{a} + \frac{\dot{a}^2 + k}{a^2} \right) \\
%
&g^{\mu\nu}T_{\mu\nu} = \underbrace{g^{\mu\nu}u_\mu u_\nu}_{1} \left( \rho + P \right) - P \underbrace{g^{\mu\nu}g_{\mu\nu}}_4 = \rho - 3P \\
%
&\Rightarrow  6\left( \frac{\ddot{a}}{a} + \frac{8\pi G \rho}{3} \right) = 8 \pi G\left( \rho - 3P \right)\\
%
&\Rightarrow \frac{\ddot{a}}{a} = -\frac{4\pi G}{3}\left( \rho + 3P \right)
%
\end{align*}                                                          
%
\end{namebox}



\section{Inflation derivations}



\begin{namebox}{Euler-Lagrange equations for the inflaton}{appn:cosm:EL}
%
Starting from the action in \myref{cosm:lagrangian}:
%
\begin{gather*}                                                       
%
S = \int d^4 x \underbrace{\sqrt{|g|} \left( \mathcal{L}_g + \mathcal{L}_\phi \right)}_{\mathcal{L}} \sts g = -a^6 \ ,\  \mathcal{L}_g = \frac{1}{2}\frac{\mathcal{R}}{k_G} \ ,\ \mathcal{L}_\phi = \frac{1}{2}\partial_\mu \phi \partial^\mu \phi - V
%
\end{gather*}                                                         
%
Applying the Euler-Lagrange (EL) equation $\left(\frac{\partial\mathcal{L}}{\partial \phi} - \partial_\mu \frac{\partial \mathcal{L}}{\partial \left(\partial_\mu \phi\right)} = 0\right)$ and remembering the scale factor is a function of just time:
%
\begin{align*}                                                        
%
\frac{\partial\mathcal{L}}{\partial \phi} &= -\sqrt{|g|} V_{, \phi} \sts X_{, \phi} \equiv \frac{\partial X}{\partial \phi} \\
%
\partial_\mu \frac{\partial \mathcal{L}}{\partial \left(\partial_\mu \phi\right)} &= \partial_\mu \left( \sqrt{|g|} \partial^\mu \phi \right) = \sqrt{|g|}\partial_\mu \partial^\mu \phi + 3H\sqrt{|g|}\delta_{t\mu}\partial^\mu \phi \\
%
(EL) &\Rightarrow \ddot{\phi} + 3 H \dot{\phi} + V_{,\phi} = 0
%
\end{align*}  
%
\end{namebox}



\begin{namebox}{Scalar-vector-tensor decomposition of $\delta\mathbf{g}$}{appn:cosm:svt}
%
A spacetime metric must be symmetric, so it can have at most ten different components:
%
\begin{align*}
%
g_{\mu\nu} &= \bar{g}_{\mu\nu} + \delta g_{\mu\nu} && \ \\
%
\delta g_{00} &= 2a^2 \varphi && \sts \varphi \text{ is a scalar} \\
%
\delta g_{0i} &= a^2(B_{, i} + S_i) && \sts B \text{ is a scalar, } \svec{S} \text{ is a 3-vector with } S^i_{\ , i} = 0 \\
%
\delta g_{ij} &= a^2(2\psi \delta_{ij} + 2E_{,ij}) && \sts \psi, E \text{ are scalars} \\
%
& \ \ + a^2(F_{i,j} + F_{j,i}) && \sts \svec{F} \text{ is a 3-vector with } F^i_{\ , i} = 0 \\
%
& \ \ + a^2h_{ij} && \sts \mathbf{h} \text{ is a 3-tensor with } h^i_{\ i} = 0 \text{ and } h^i_{\ j,i} = 0
%
\end{align*}
%
\begin{align*}
%
\mathbf{g}^{-1} &= (\bar{\mathbf{g}} + \delta\mathbf{g})^{-1} = \sum^{\infty}_{k=0} \left( -\bar{\mathbf{g}}^{-1} \delta \mathbf{g} \right)^k \bar{\mathbf{g}}^{-1} \approx \bar{\mathbf{g}}^{-1} - \bar{\mathbf{g}}^{-1} \left(\delta\mathbf{g}\right)\bar{\mathbf{g}}^{-1} \\ 
%
\therefore \ g^{\mu\nu} &= \bar{g}^{\mu\nu} - \bar{g}^{\mu\alpha}\bar{g}^{\beta\nu}\delta g_{\alpha\beta} 
%
\end{align*}
%
The last first-order equality shows how to raise the indices of the perturbation.
%
Below are a few more relations I worked out that come in handy:
%
\begin{gather*}
%
g \rightarrow\det\left( \bar{\mathbf{g}} + \delta \mathbf{g} \right) = \bar{g}\left( 1 + \bar{g}^{\mu\nu}\delta g_{\mu\nu}\right) = \bar{g}\left( 1 + \bar{g}^{\mu\nu}\delta g^{scalar}_{\mu\nu}\right) \\
%
\frac{\partial_\mu \sqrt{-g}}{\sqrt{-g}} \rightarrow \frac{1}{2}\left( \frac{\partial_\mu \bar{g}}{\bar{g}} + \partial_\mu \gamma \right)
%
\sts \gamma \equiv \bar{g}^{\mu\nu}\delta g^{scalar}_{\mu\nu}
%
\end{gather*}
%
\end{namebox}



\begin{namebox}{Gauge choice}{appn:cosm:gauge}
%
As I described in \myref{appn:cosm:svt} there are ten seemingly independent components to the metric. 
%
But there is some gauge redundancy; Let's perform an infinitesimal change of coordinates to $\bar{\mathbf{g}} + \delta \mathbf{g}$ and track the result to first order: 
%
\begin{gather*}
%
\text{For } \tilde{\vec{x}} = \vec{x} + \vec{\xi} \sts \vec{\xi} \ll \vec{1} \ \ \ \ \ \tilde{g}_{\mu\nu}(\tilde{\vec{x}}) = \frac{\partial x^\alpha}{\partial\tilde{x}^\mu} \frac{\partial x^\beta}{\partial\tilde{x}^\nu} g_{\alpha\beta}(\vec{x})
%
\end{gather*}
%
\begin{align*}
%
\tilde{g}_{\mu\nu}(\tilde{\vec{x}}) &= \left(\delta^\alpha_{\ \mu} - \frac{\partial\xi^\alpha}{\partial\tilde{x}^\mu} \right)\left(\delta^\beta_{\ \nu} - \frac{\partial\xi^\beta}{\partial\tilde{x}^\nu} \right) (\bar{g}_{\alpha\beta}(\vec{x}) + \delta g_{\alpha \beta}(\vec{x})) \\ 
%
\tilde{g}_{\mu\nu}(\tilde{\vec{x}}) &= \left(\delta^\alpha_{\ \mu} - \frac{\partial\xi^\alpha}{\partial\tilde{x}^\mu} \right)\left(\delta^\beta_{\ \nu} - \frac{\partial\xi^\beta}{\partial\tilde{x}^\nu} \right) (\bar{g}_{\alpha\beta}(\tilde{\vec{x}} - \vec{\xi}) + \delta g_{\alpha \beta}(\tilde{\vec{x}} - \vec{\xi})) \\
%
\tilde{g}_{\mu\nu}(\tilde{\vec{x}}) &= \bar{g}_{\mu\nu}(\tilde{\vec{x}}) - \bar{g}_{\mu\beta}(\tilde{\vec{x}})\frac{\partial \xi^\beta}{\partial\tilde{x}^{\nu}} - \bar{g}_{\alpha\nu}(\tilde{\vec{x}})\frac{\partial \xi^\alpha}{\partial\tilde{x}^{\mu}} - \bar{g}_{\mu\nu,\gamma}(\tilde{\vec{x}})\xi^\gamma + \delta g_{\mu\nu}(\tilde{\vec{x}}) \\
%
& \text{Or written in a more condensed way:} \\
%
g_{\mu\nu} &\rightarrow \bar{g}_{\mu\nu} + \delta g_{\mu\nu} - \bar{g}_{\mu\beta} \xi^\beta_{\ ,\nu} - \bar{g}_{\alpha\nu}\xi^\alpha_{\ ,\mu} - \bar{g}_{\mu\nu,\gamma}\xi^\gamma = \bar{g}_{\mu\nu} + \tilde{\delta g}_{\mu\nu}
%
\end{align*}
%
If we decompose $\vec{\xi}$:
%
\begin{align*}
%
\xi^\mu &= \delta^\mu_{\ 0}\xi^0 + \delta^\mu_{\ i}\left(\chi^i + \varsigma^{, i}\right) \\
%
& \ \ \sts \xi^0, \varsigma \text{ are scalars, } \chi^i \text{ is a three vector with } \chi^i_{\ ,i} = 0 \\ 
%
\delta \tilde{g}_{00} &= \delta g_{00} - 2a(a\xi^{0})\p \\
%
\delta \tilde{g}_{0i} &= \delta g_{0i} + a^2\left( \chi_i\p + (\varsigma\p - \xi^0)_{, i} \right) \\
%
\delta \tilde{g}_{ij} &= \delta g_{ij} + a^2\left( 2\frac{a\p}{a}\delta_{ij}\xi^0 + 2\varsigma_{, ij} + (\chi_{i,j} + \chi_{j,i}) \right)\\
%
& \ \ \text{Where } X\p \equiv \frac{\partial X}{\partial \eta} 
%
\end{align*}
%
We can pick a $\vec{\xi}$ that nullifies four out of the ten independent components.
%
That's because the Bianchi identity provides four constraints so the redundancy of writing six degrees of freedom with ten components is returned to us as things we can change with a gauge choice.
%
Two such gauges are the \newterm{Newtonian gauge} (\myref{appn:cosm:invars}) and the \newterm{spatially flat gauge} (\myref{appn:cosm:invars2}).
%
\end{namebox}




\begin{namebox}{Scalar-vector-tensor decomposition on $\delta\mathbf{T}$}{appn:cosm:svt2}
%
Following \cite{book:Weinberg:2008} I perturb a perfect fluid's stress-energy tensor including adding anisotropic stress terms.
%
Anisotropic stress will have a scalar, a vector and a tensor component:
%
\begin{align*} 
%
T_{\mu\nu} &= (\epsilon + p)u_{\mu}u_{\nu} - pg_{\mu\nu} + \delta_\mu^{\ i}\delta_\nu^{\ j}\Pi_{ij}\\ 
%
&\sts \Pi_{ij} \equiv a^2\left( \partial_i\partial_j\pi^S + \left(\partial_i\pi^V_j + \partial_ji\pi^V_i\right) + \pi^T_{ij}  \right) \\ 
%
&\sts \pi^S \text{ is a scalar} \\
%
&\sts \pi^V \text{ is a vector with } \partial_i\pi^V_i = 0 \\
%
&\sts \pi^T \text{ is a symmetric tensor with } \partial_i\pi^T_{ij} = 0 \text{ and } \pi^T_{ii} = 0 
%
\end{align*}
%
\begin{align*}
%
\mathbf{g} &= \bar{\mathbf{g}} + \delta\mathbf{g} \\
%
\epsilon &= \bar{\epsilon} + \delta \epsilon && \sts \delta \epsilon \text{ is a scalar}\\
%
p &= \bar{p} + \delta p && \sts \delta p \text{ is a scalar}\\
%
u_\mu &= \bar{u}_\mu + \delta u_\mu && \sts \bar{u}_\mu = \delta^0_{\ \mu}\bar{g}_{00}, \ \delta u_\mu = \delta^0_{\ \mu}\delta u_0 + \delta^i_{\ \mu}\left( v_i + z_{,i} \right) \\
%
& \ && \sts u_0, z \text{ are a scalars, } \svec{v} \text{ is a three vector with } v^i_{\ , i} = 0 \\
%
& \text{Enforcing} && g^{\mu\nu} u_\mu u_\nu = 1 
%
\Rightarrow \delta u_0 = \frac{1}{2} \frac{\delta g_{00}}{a}
%
\end{align*}
%
Expanding the perturbation: 
%
\begin{align*}
%
\delta T_{00} &= \bar{\epsilon}\delta g_{00} + \bar{g}_{00}\delta \epsilon \\
%
\delta T_{0i} &= \bar{g}_{00}(\bar{\epsilon} + \bar{p})(v_i + z_{,i}) -\bar{p}\delta g_{0i}\\
%
\delta T_{ij} &= -\bar{p}\delta g_{ij} - \delta p \bar{g}_{ij} + a^2\Pi_{ij}
%
\end{align*}
%
Raising an index yields:
%
\begin{align*}
%
T^{\mu}_{\ \nu} &= \left( \bar{g}^{\mu\alpha} + \delta g^{\mu\alpha} \right)\left( \bar{T}_{\alpha\nu} + \delta T_{\alpha\nu} \right) \\ 
%
\Rightarrow \delta T^\mu_{\ \nu} &= \bar{g}^{\mu\alpha}\delta T_{\alpha\nu} + \delta g^{\mu\alpha}\bar{T}_{\alpha\nu} = \bar{g}^{\mu\alpha}\delta T_{\alpha\nu} - \bar{g}^{\mu\sigma}\bar{g}^{\alpha\rho}\delta g_{\sigma\rho}\bar{T}_{\alpha\nu}
%
\end{align*}
%
\end{namebox}



\begin{namebox}{Gauge choice: Newtonian gauge}{appn:cosm:invars}
%
Gauge freedom allows us to pick convenient coordinates in which hard math becomes easier, but it also requires us to define \newterm{gauge-invariant} quantities to understand the real-world impacts of derived quantities.
%
From the scalar-perturbed metric I assert without proof that the following quantities are gauge-invariant%
%
\footnote{This is from chapter 7 of Mukhanov's {\it Physical Foundations of Cosmology}\cite{book:Mukhanov:2005}.}:
%
\begin{align*}
%
ds^2 &= a^2\left((1 + 2\varphi)d\eta^2 + 2B_{,i}dx^id\eta - \left((1 - 2\psi)\delta_{ij} - 2E_{,ij}\right)dx^i dx^j\right) \\
%
\Phi &\equiv \varphi - \frac{1}{a}\left( a\left( B - E\p \right)\right)\p \\ 
%
\Psi &\equiv \psi + \frac{a\p}{a}\left( B - E\p \right)
%
\end{align*}
%
The Newtonian gauge is picking a $\vec{\xi}$ such that $B = E = 0, \ \varphi = \Phi, \ \psi = \Psi$. This simplifies the metric to: 
%
\begin{gather*}
%
\delta g_{scalar}^{\mu\nu} = -2a^{-2}\left(\delta_{\ 0}^{\mu}\delta_{\ 0}^{\nu} \Phi + \delta_{\ i}^{\mu}\delta_{\ i}^{\nu} \Psi \right) \ ,\ \ \ \delta g^{scalar}_{\mu\nu} = 2a^2\left(\delta^0_{\ \mu}\delta^0_{\ \nu} \Phi + \delta^i_{\ \mu}\delta^j_{\ \nu} \Psi \right) \\ 
%
\gamma \equiv \bar{g}^{\mu\nu}\delta g_{\mu\nu}^{scalar} = 2\Phi - 6\Psi 
%
\end{gather*}
%
The Newtonian gauge is useful because the scalar perturbations that resembles Newtonian potentials ($\varphi$ and $\psi$) equal the respective gauge invariant quantities $\Phi$ and $\Psi$.
%
So if you want to compute these very natural measures of scalar perturbation, do it in the simple Newtonian gauge.
%
\end{namebox}



\begin{namebox}{Gauge choice: spatially flat gauge}{appn:cosm:invars2}
%
As I said in \myref{appn:cosm:invars}, the Newtonian gauge is great for computing $\Phi$ and $\Psi$.
%
There is another gauge that comes in handy where even the $dx^idx^j$ terms in the perturb metric appear flat.
%
Looking at the scalar perturbed metric in \myref{appn:cosm:gauge}:
%
  \begin{align*}
%
    ds^2 &= a^2\left((1 + 2\varphi)d\eta^2 + 2B_{,i}dx^id\eta - \left((1 - 2\psi)\delta_{ij} - 2E_{,ij}\right)dx^i dx^j\right) \\
%
    &\text{We pick } \vec{\xi} \text{ in } x^{\mu}_{old} = x^{\mu} + \xi^{\mu} \ , \ \ \xi^{\mu} = \delta^{\mu}_{\ 0}\xi^0 + \delta^{\mu}_{\ i}\left(\chi^i + \varsigma^{,i} \right) \sts \\
%
    \delta_{ij} &= (1 - 2\psi^{old})\delta_{ij} - 2E^{old}_{,ij} + 2\mathcal{H}\delta_{ij}\xi^0 + 2\varsigma_{, ij} + (\chi_{i,j} + \chi_{j,i}) \\ 
%
    \Rightarrow ds^2 &= a^2\left((1 + 2\varphi)d\eta^2 + 2B_{,i}dx^id\eta - \delta_{ij}\right) \\
%
    &\text{Or if you want to use the unperturbed cosmological time:} \\
%
    ds^2 &= (1 + 2\varphi)dt^2 + 2aB_{,i}dx^idt - a^2\delta_{ij}dx^i dx^j
%
  \end{align*}
%
This last metric matches Dodelon's {\it Modern Cosmology}\cite{book:Dodelson:2003} notation up to the metric signature in his section (6.5.3).
%
The weird thing about this metric is that its perturbation has zeros along the bottom block which means it's not invertible.
%
However, since $\delta g^{\mu\nu}$ is not the inverse of $\delta g_{\mu\nu}$ I suppose that's okay.
%
Here some useful quantities are:
%
\begin{gather*}
%
\gamma = 2\varphi \ ,\ \ \ \partial_\mu \gamma = 2\varphi_{, \mu} \\ 
%
\delta g^{\mu\nu} = -2\varphi \delta^{0}_{\ \mu} \delta^0_{\ \nu} \Rightarrow \partial_\mu \delta g^{\mu\nu} = -2\dot{\varphi} \delta^\nu_{\ 0} 
%
\end{gather*}
%
\end{namebox}



\begin{namebox}{Scalar-vector-tensor decomposition for $\phi$}{appn:cosm:svt4}
%
This box shows that the inflaton's equation of motion just involves scalar modes to first order.
%
Therefore we can consider perturbations of $\phi$ and scalar perturbations of $\mat{g}$ togehter, but without concerning ourselves with the vector and tensor perturbations of $\mat{g}$.
%
Starting from the equation of motion:
%
\begin{gather*}
%
\frac{1}{\sqrt{-g}}\partial_\mu\left( \sqrt{-g} g^{\mu\nu} \partial_\nu \phi\right) + V_{,\phi}(\phi) = 0
%
\end{gather*}
%
Aready I showed that the determinant of the metric is only perturbed by the trace of the scalar component (a quantity I call $\gamma$) in \myref{appn:cosm:svt}.
%
Continuing:
%
\begin{align*}
%
\partial_\mu g^{\mu\nu}\partial_\nu \phi &= \partial_\mu\bar{g}^{\mu\nu}\partial_\nu(\bar{\phi} + \delta \phi) + \partial_\mu\delta g^{\mu\nu}\partial_\nu \bar{\phi} = \partial_\mu\bar{g}^{\mu\nu}\partial_\nu(\bar{\phi} + \delta \phi) + \partial_\mu\delta g^{\mu 0}\partial_0 \bar{\phi}\\  
%
&= \partial_\mu\bar{g}^{\mu\nu}\partial_\nu(\bar{\phi} + \delta \phi) + \left(\partial_\mu\delta g^{\mu 0}_{scalar} + \cancel{\partial_i\delta g^{i 0}_{vector}} + \partial_\mu\cancel{\delta g^{\mu 0}}_{tensor}\right)\partial_0 \bar{\phi} \\
%
g^{\mu\nu}\partial_\mu\partial_\nu \phi &= \bar{g}^{\mu\nu}\partial_\mu\partial_\nu\bar{\phi} + \bar{g}^{\mu\nu}\partial_\mu\partial_\nu\delta \phi + \delta g^{\mu\nu}\partial_\mu\partial_\nu\bar{\phi} \\
%
&= \bar{g}^{\mu\nu}\partial_\mu\partial_\nu\bar{\phi} + \bar{g}^{\mu\nu}\partial_\mu\partial_\nu\delta \phi + \left(\delta g^{00}_{scalar}\right)\partial_0\partial_0\bar{\phi}
%
\end{align*}
%
Only scalar modes here! 
%
\end{namebox}



\begin{namebox}{Negative $\eta$ during inflation}{appn:cosm:negeta}
%
This box is to justify negative $\eta$.
%
Suppose the scale factor features exponential growth:
%
\begin{gather*}
%
a(t) = e^{Ht} \Rightarrow \frac{da}{dt} = Ha \frac{1}{a}{da}{d\eta} = Ha \Rightarrow a(\eta) = \frac{1}{c - H\eta} \\
%
\text{Convention sets } a(t_0) = 1 \sts t_0 = \text{ today} \\
%
\Rightarrow a(t = 0) \rightarrow 0 \Rightarrow c = 0
%
\end{gather*}
%
Thus $\eta$ is negative.
%
\end{namebox}



\begin{namebox}{Solving for $\delta \phi$ --- part 1}{appn:cosm:deltaphi_sol1}
%
Because of SVT decomposition, we are free to consider the field and scalar perturbations in isolation.
%
We do not need to use the decomposition of the stress-energy tensor from \myref{appn:cosm:svt2} because we can instead get the dynamics directly from the scalar field's Euler-Lagrange equations rather than using the Einstein equations.
%
To do so I can pick the spatially flat gauge (\myref{appn:cosm:invars2}) and I choose to first use cosmological time before switching to conformal time.
%
Let's breakdown this expansion into more managable parts:
%
\begin{align*}
%
0 &= \frac{1}{\sqrt{-g}}\partial_\mu\left( \sqrt{-g} g^{\mu\nu} \partial_\nu \phi\right) + V_{,\phi}(\phi) \\ 
%
0 &= \underbrace{\frac{\partial_\mu\sqrt{-g}}{\sqrt{-g}}\left( g^{\mu\nu} \partial_\nu \phi\right)}_{\text{term1}}
%
+ \underbrace{\partial_\mu g^{\mu\nu} \left(\partial_\nu \phi\right)}_{\text{term2}}
%
+ \underbrace{g^{\mu\nu} \partial_\mu \partial_\nu \phi}_{\text{term3}}
%
+ \underbrace{V_{,\phi}(\phi)}_{\text{term4}} \\
%
\text{term1}: \ \ & \frac{\partial_\mu\sqrt{-g}}{\sqrt{-g}}\left( g^{\mu\nu} \partial_\nu \phi\right) \\ 
%
& = \frac{1}{2}\frac{\partial_\mu \bar{g}}{\bar{g}}\left( \bar{g}^{\mu\nu} \partial_\nu \bar{\phi} + \bar{g}^{\mu\nu} \partial_\nu \delta \phi + \delta g^{\mu\nu} \partial_\nu \bar{\phi} \right) + \frac{\partial_\mu \gamma}{2}\bar{g}^{\mu\nu}\partial_\nu \bar{\phi} \\
%
&= 3H\left( \dot{\bar{\phi}} + \dot{\delta \phi} - 2\varphi \dot{\bar{\phi}} \right) + \dot{\varphi}\dot{\bar{\phi}} \\
%
\text{term2}: \ \ & \partial_\mu g^{\mu\nu} \left(\partial_\nu \phi\right) \\ 
%
& = \left(\partial_\mu \bar{g}^{\mu\nu}\right)\partial_\nu \bar{\phi} + \left(\partial_\mu \bar{g}^{\mu\nu}\right)\partial_\nu \delta \phi + \left(\partial_\mu \delta g^{\mu\nu}\right)\partial_\nu \bar{\phi} \\
%
&= -2\dot{\varphi}\dot{\bar{\phi}} \\
%
\text{term3}: \ \ & g^{\mu\nu}\partial_\mu \partial_\nu \phi \\
%
& = \bar{g}^{\mu\nu}\partial_\mu \partial_\nu \bar{\phi} + \bar{g}^{\mu \nu} \partial_\mu \partial_\nu \delta \phi + \delta g^{\mu\nu}\partial_\mu \partial_\nu\bar{\phi} \\
%
&= \ddot{\bar{\phi}} + \ddot{\delta \phi} - \frac{1}{a^2}\nabla^2\delta\phi - 2\varphi\ddot{\bar{\phi}} \\
%
\text{term4}: \ \ & V_{,\phi}(\phi) = V_{,\phi}\left(\bar{\phi}\right) + V_{,\phi\phi}\left(\bar{\phi}\right)\delta\phi
%
\end{align*}
%
Combining these terms we get:
%
\begin{align*}
%
0 &= -6H\varphi\dot{\bar{\phi}} - \dot{\varphi}\dot{\bar{\phi}} - 2\varphi\ddot{\bar{\phi}} \\ 
%
& \ \ + \ddot{\delta \phi} + 3H\dot{\delta \phi} - \frac{1}{a^2}\nabla^2 \delta \phi + V_{,\phi\phi}\delta \phi \\
%
& \ \ + \cancel{\text{classical terms}} 
%
\end{align*}
%
\end{namebox}



\begin{namebox}{Solving for $\delta \phi$ --- part 2}{appn:cosm:deltaphi_sol2}
%
The slow-roll condition is that $\dot{\bar{\phi}}$ is small --- in fact, let's treat it as first order too --- and from the chaotic inflation attractor solutions I gave in \myref{cosm:attractors} it's clear that $\ddot{\bar{\phi}}$ is very small.
%
I can drop the relevant terms.
%
So though I was careful to expand both $\phi$ and $\mathbf{g}$ to ensure I capture any backreactions between the two, $\delta \phi$ evolves as through in unperturbed FLRW spacetime to first order.
%
We can switch to Fourier space%
\footnote{The only notational hint I give is giving $\svec{k}$ as the arugment instead of $\svec{x}$.
%
If the argument is not shown figure it out from context.}.
%
Thus $\nabla^2 \rightarrow -k^2 \sts\ k^2 \equiv \svec{k}\cdot\svec{k}$.
%
Making this transformation yields:
%
\begin{align*}
%
0 &= \ddot{\delta \phi} + 3H\dot{\delta \phi} - \frac{1}{a^2}\nabla^2 \delta \phi + V_{,\phi\phi}\delta \phi \\
%
& \ \ \delta \phi(t, \svec{x}) = \int \frac{d^3k}{(2\pi)^3} e^{i\svec{k}\cdot\svec{x}} \delta \phi\left(t, \svec{k}\right) \\
%
&= \ddot{\delta \phi} + 3H\dot{\delta \phi} + \left(\frac{k^2}{a^2} + V_{,\phi\phi}\right)\delta \phi
%
\end{align*}
%
I can switch to conformal time:
%
\begin{align*}
%
0 &= \delta \phi\pp + 2\mathcal{H}\delta \phi\p + \left(k^2 + a^2 V_{,\phi\phi}\right)\delta \phi \\
%
& \ \ \text{For } u \equiv a\delta\phi \sts \begin{array}{ll} \delta\phi\p &= \frac{u\p}{a} - \mathcal{H}\frac{u}{a} \\ \delta\phi\pp &= \frac{u\pp}{a} - 2\mathcal{H}\frac{u\p}{a} + \left( 2\mathcal{H}^2 - \frac{a\pp}{a}\right)\left( \frac{u}{a} \right) \end{array} \\
%
0 &= u\pp + \left(k^2 + a^2 V_{,\phi\phi} - \frac{a\pp}{a}\right) u \\
%
& \ \ \text{For } s \equiv \frac{u}{\sqrt{-\eta}} \sts (-\eta)^{3/2}u\pp = \eta^2 s\pp + \eta s\p - \frac{1}{4}s \\
%
0 &= \eta^2 s\pp + \eta s\p + \left(\eta^2\left( k^2 - a^2 V_{,\phi\phi} - \frac{a\pp}{a} \right) - \frac{1}{4}\right)s \\
%
\end{align*}
%
This almost looks like the equation defining a Bessel function of $s = a\delta\phi/\sqrt{-\eta}$.
%
In the next part, I can use slow-roll approximations to further simplify an solsve this expression.
%
\end{namebox}



\begin{namebox}{Solving for $\delta \phi$ --- part 3}{appn:cosm:deltaphi_sol3}
%
Resuming from \myref{appn:cosm:deltaphi_sol2}:
%
\begin{align*}
%
0 &= \eta^2 s\pp + \eta s\p + \left(\eta^2\left( k^2 - a^2 V_{,\phi\phi} - \frac{a\pp}{a} \right) - \frac{1}{4}\right)s \\
%
& \ \ \text{Noting } \epsilon_{SR} = -\frac{\dot{H}}{H^2} \\ 
%
& \ \ \eta = \int \frac{dt}{a} = \int \frac{1}{H}\left(\frac{d}{da}\frac{1}{a}\right)da = \frac{1}{aH}\sum_{n=0}^{\infty}\left(-\epsilon_{SR}\right)^n \\ 
%
& \ \Rightarrow \eta^2 a^2 = \frac{1}{H^2}\left(1 - \epsilon_{SR} + \mathcal{O}(\epsilon_{SR}^2)\right) \\
%
& \ \ \text{Noting } \frac{a\pp}{a} = \dot{a}^2 + a\ddot{a} = 2a^2 H^2 \left( 1 - \frac{\epsilon_{SR}}{2}\right) \\
%
& \ \Rightarrow \frac{a\pp}{a} \eta^2 = 2 - 3\epsilon_{SR} + \mathcal{O}(\epsilon_{SR}^2) \\
%
0 &= \eta^2 s\pp + \eta s\p + \left((\eta k)^2 - \frac{V_{,\phi\phi}}{H^2} - \left(\frac{9}{4} - 3\epsilon_{SR}\right)\right)s \\
%
& \ \ \text{Noting } \frac{V_{,\phi\phi}}{H^2} \approx 3\eta_{SR} \\ 
%
0 &= \eta^2 s\pp + \eta s\p + \left((\eta k)^2 - \alpha^2\right)s \rightarrow 0 = z^2 \frac{d^2s}{dz^2} + z \frac{ds}{dz} + \left( z^2 - \alpha^2 \right)s  
%
\end{align*}
%
Where if $k$ is independent of $\eta, \ z \equiv -k\eta = k|\eta|$, $\alpha \approx \sqrt{\frac{9}{4} + 3\eta_{SR} - 3\epsilon_{SR}} \approx \frac{3}{2} + \eta_{SR} - \epsilon_{SR}$ .
%
Keeping the $\eta_{SR}$ and $\epsilon_{SR}$ terms is to track an order smaller than the rest of the terms so we can approximate $\alpha = 3/2$ for quantization.
%
This gives Bessel functions: $$\delta \phi^{\pm}_{k}(\eta) = \frac{\sqrt{-\eta}}{a} J_{\pm 3/2}(k|\eta|)$$
%
\end{namebox}
%

%
\begin{namebox}{Quantizing $\delta \phi$ --- part 1}{appn:cosm:deltaphi_quantize1}
%
In \myref{appn:cosm:deltaphi_sol3} I found the two solutions $\delta \phi^{\pm}_{\svec{k}}(\eta)$ which means that I can create solutions like:
%
\begin{align*}
%
\delta \phi_k(\eta) &= \frac{\sqrt{-\eta}}{a}\left(C^{(1)}_k J_{}(k|\eta|) + C^{(2)}_k J_{-3/2}(k|\eta|) \right) &&\sts C^{(1)}_k, C^{(2)}_k \in \mathbb{C} \ \forall k
%
\end{align*}
%
We can better constrain these constants by quantizing and then imposing that $\delta \phi$ commutes with its canonical momentum, $\delta \pi$, following the uncertainty principle.
%
Because $\delta \phi(\vec{x}) \in \mathbb{R} \ \forall \vec{x}$, an arbitrary linear combination must look like this:
%
\begin{align*}
%
\delta\phi(\vec{x}) &= \frac{1}{2} \int \frac{d^3 \svec{k}}{(2\pi)^3} e^{i\svec{k}\cdot\svec{x}}\left( A_{\svec{k}}\delta\phi(k, \eta) + \left(A_{-\svec{k}}\delta\phi(k, \eta)\right)^* \right) \sts A_{\svec{k}} \in \mathbb{C} \ \forall \svec{k} \\
%
\delta \pi\left(\vec{y}\right) &\equiv \frac{\partial \mathcal{L}}{\partial\left( \partial_0 \delta\phi \right)}|_{\vec{y}} = \frac{\sqrt{-g}}{a^2}\partial_0 \phi(\vec{y}) + \mathcal{O}\left(\epsilon_{SR} \right) \approx a^2 \delta\phi\p(\vec{y}) + \text{ abelian term} \\
%
&= \frac{a^2}{2} \int \frac{d^3 \svec{p}}{(2\pi)^3} e^{i\svec{p}\cdot\svec{y}} \left( A_{\svec{p}}\delta\phi\p(p, \eta) + \left(A_{-\svec{p}}\delta\phi\p(p, \eta)\right)^* \right)
%
\end{align*}
%
I want to promote Fourier coefficients $\left(A_{\svec{k}}, A^*_{-\svec{k}}\right)$ to quantum raising and lowering operators $\left(\hat{a}^+_{\svec{k}}, \hat{a}^-_{-\svec{k}}\right)$ such that: 
%
\begin{align*}
%
\left[ \hat{\delta \phi}(\vec{x}), \hat{\delta\pi}(\vec{y}) \right] &= i \delta^4(\vec{x} - \vec{y}) \\ 
%
&= \frac{a^2}{4} \int \frac{d^3 \svec{k}}{(2\pi)^3}\frac{d^3 \svec{p}}{(2\pi)^3} e^{i\svec{k}\cdot\svec{x}} e^{i\svec{p}\cdot\svec{y}} \\
%
& \ \ \times \left[ \left( \hat{a}^+_{\svec{k}}\delta\phi_k + \hat{a}^-_{-\svec{k}}\delta\phi_k^* \right), \left( \hat{a}^+_{\svec{p}}\delta\phi\p_p + \hat{a}^-_{-\svec{p}}\left(\delta\phi\p_p\right)^* \right) \right] \\
%
&= \frac{a^2}{4} \int \frac{d^3 \svec{k}}{(2\pi)^3}\frac{d^3 \svec{p}}{(2\pi)^3} e^{i\svec{k}\cdot\svec{x}} e^{i\svec{p}\cdot\svec{y}} \\
%
& \ \ \times \left( \delta\phi_k \delta\phi\p_p \left[ \hat{a}^+_{\svec{k}}, \hat{a}^+_{\svec{p}} \right] + \delta\phi_k^* \delta\phi\p_p \left[ \hat{a}^-_{-\svec{k}}, \hat{a}^+_{\svec{p}} \right] \right. \\
%
& \ \ \ \ + \left. \delta\phi_k \left(\delta\phi\p_p\right)^* \left[ \hat{a}^+_{\svec{k}}, \hat{a}^-_{-\svec{p}} \right] + \delta\phi_k^* \left(\delta\phi\p_p\right)^* \left[ \hat{a}^-_{-\svec{k}}, \hat{a}^-_{-\svec{p}} \right] \right) \\ 
%
& \ \ \text{Setting } \left[ \hat{a}^-_{\svec{k}}, \hat{a}^+_{\svec{p}} \right] = \left( 2\pi \right)^3 \delta^3(\svec{k} - \svec{p}) \\  
%
&= \frac{a^2}{4} \int \frac{d^3 \svec{k}}{(2\pi)^3} e^{i\svec{k}\cdot\left(\svec{x} - \svec{y}\right)} \left(\delta\phi_k^* \delta\phi\p_k - \delta\phi_k \left(\delta\phi\p_k\right)^* \right) \\ 
%
\Rightarrow \frac{4i}{a^2} &= \delta\phi_k^* \delta\phi\p_k - \delta\phi_k \left(\delta\phi\p_k\right)^* \\
%
\end{align*}
%
In the next part comes a trick with a Wronskian to wrangle this expression into one more explicitly constraining $C_k$.
%
\end{namebox}



\begin{namebox}{Quantizing $\delta \phi$ --- part 2}{appn:cosm:deltaphi_quantize2}
%
Resuming from:
%
\begin{align*}
%
\Rightarrow \frac{4i}{a^2} &= \delta\phi_k^* \delta\phi\p_k - \delta\phi_k \left(\delta\phi\p_k\right)^* \\
%
& \ \ \text{Rewriting in terms of the Wronskian of the Bessel solutions} \\
%
\Rightarrow \frac{4i}{|\eta|} &= k(D - D^*) W(J_{3/2}(k|\eta|), J_{-3/2}(k|\eta|)) \sts  D \equiv C^{(1)*}C^{(2)} \\
%
& \ \ \text{Abel's Identity gives } W(J_{3/2}(k|\eta|), J_{-3/2}(k|\eta|)) = \frac{c_W}{k|\eta|} \\
%
& \sts c_W \in \mathbb{R} \Rightarrow c_W = W(J_{3/2}(1), J_{-3/2}(1)) = \frac{2}{\pi} \\ 
%
\Rightarrow \frac{4i}{|\eta|} &= k(D - D^*) \frac{2}{\pi k |\eta|} \Rightarrow 2i\pi = D - D^*
%
\end{align*}
%
This adds a constraint on the imaginary part of $D$, which in turn {\it partially} constrains $C^{(1)}$ and $C^{(2)}$.
%
So enforcing this quantization condition did not uniquely define a vacuum.
%
We can pick the vacuum of our choice, the \newterm{Bunch-Davies} vacuum (its additional constraints not shown here), $C^{(1)} = \sqrt{\pi}, \ C^{(2)} = iC^{(1)}$ which also satisfies the quantization condition.
%
We now have a quantized field such that: 
%
\begin{align*}
%
\hat{\delta \phi}\left(\vec{x} = (\eta, \svec{x})\right) &= \int \frac{d^3\svec{k}}{(2\pi)^3} e^{i\svec{k}\cdot\svec{x}} \frac{\sqrt{-\pi\eta}}{2a}\left( \hat{a}^+_{\svec{k}}H_{3/2}(k|\eta|) + \hat{a}^-_{-\svec{k}}H^*_{3/2}(k|\eta|) \right) \\
%
&=  \frac{\sqrt{-\pi\eta}}{2a} \int \frac{d^3\svec{k}}{(2\pi)^3} \hat{a}^+_{\svec{k}} \left(e^{i\svec{k}\cdot\svec{x}} H_{3/2}(k|\eta|) + \hat{a}^-_{\svec{k}} e^{-i\svec{k}\cdot\svec{x}} H^*_{3/2}(k|\eta|)\right) \\
%
& \ \ \sts H_{3/2} \equiv \text{ (Hankel function)}^{(1)}_{3/2}
%
\end{align*}
%
\end{namebox}
%

%
\begin{namebox}{Finding $\delta \phi$'s spectrum}{appn:cosm:deltaphi_spectrum}
%
To go from the quantization of $\delta \phi$ finished in \myref{appn:cosm:deltaphi_quantize2} to its expected spectrum we compute its vacuum-to-vaccum expectation values:
%
\begin{align*}
%
\hat{\delta \phi}(\eta, \svec{x}) \ket{0_{BD}} &= \frac{\sqrt{\pi|\eta|}}{2a}\int \frac{d^3 \svec{k}}{(2\pi)^3} e^{i\svec{k}\cdot\svec{x}} H_{3/2}(k|\eta|)\ket{\svec{k}} \\
%
\bra{0_{BD}} \hat{\delta \phi}(\eta, \svec{x}) &= \frac{\sqrt{\pi|\eta|}}{2a}\int \frac{d^3 \svec{k}}{(2\pi)^3} e^{-i\svec{k}\cdot\svec{x}} H_{3/2}^*(k|\eta|)\bra{\svec{k}} \\
%
\bra{0_{BD}} \hat{\delta \phi}(\eta, \svec{x})\ket{0_{BD}} &= 0
%
\end{align*}
%
This last line is unsurprising.
%
That there is no mean value of $\delta \phi$ quantum fluctuations is by design when we split $\phi = \bar{\phi} + \delta \phi$.
%
But there will still be a distribution of amplitudes of fluctuations, so let's compute the variance:
%
\begin{align*}
%
\bra{0_{BD}} \hat{\delta \phi}(\eta_1, \svec{x}) \hat{\delta \phi}(\eta_2, \svec{y}) \ket{0_{BD}} &= \int \frac{d^3 \svec{p} \ d^3 \svec{k}}{(2\pi)^6} e^{i\svec{p}\cdot\svec{x}} e^{-i\svec{k}\cdot\svec{y}} \frac{\pi\sqrt{|\eta_1\eta_2|}}{4a^2} \\ & \ \ \ \ \ \times H_{3/2}(p|\eta_1|) H_{3/2}^*(k|\eta_2|) \avg{\svec{k}|\svec{p}} \\
%
&= \int \frac{d^3 \svec{k} }{(2\pi)^3} e^{i\svec{k}\cdot\left(\svec{x}-\svec{y}\right)} \frac{\pi\sqrt{|\eta_1\eta_2|}}{4a^2} \\ & \ \ \ \ \ \times H_{3/2}(k|\eta_1|) H_{3/2}^*(k|\eta_2|) \\ 
%
&= \int d(\ln(k)) k^3 e^{i\svec{k}\cdot\left(\svec{x}-\svec{y}\right)} \frac{\sqrt{|\eta_1\eta_2|}}{8a^2\pi} \\ & \ \ \ \ \ \times H_{3/2}(k|\eta_1|) H_{3/2}^*(k|\eta_2|) \\ 
%
\bra{0_{BD}} \hat{\delta \phi}(\eta, \svec{x}) \hat{\delta \phi}(\eta, \svec{x}) \ket{0_{BD}} &\equiv \int d(\ln(k)) \Delta^2_{\delta \phi}(k, \eta) \ \ \cite{Baumann:Notes}\\ 
%
\Rightarrow \Delta_{\delta \phi}(k, \eta) &= \frac{H z^{\frac{3}{2}}}{\sqrt{8\pi}} \left| H_{3/2}(z) \right| \sts z \equiv k|\eta|
%
\end{align*}
%
\end{namebox}



\begin{namebox}{Tensor perturbations are gravitational waves}{appn:cosm:grav_waves}
%
SVT decomposition allows us to consider the tensor modes in isolation.
%
So we just consider:
%
\begin{align*}
%
\mathbf{g} &= \bar{\mathbf{g}} + \delta\mathbf{g} \\ 
%
\delta g_{ij} &= a^2 h_{ij} \sts h^i_{\ i} = 0 \text{ and } h^{i}_{\ j,i} = 0 \\
%
\delta g_{\mu\nu} &= 0 \ \forall \ \mu = 0 \text{ or } \nu = 0
%
\end{align*}
%
These tensor perturbations will be interpreted as propagating gravitational waves.
%
Lets identify the direction of propagation of the perturbation at an arbitrary point with $\hat{z}$:
%
\begin{gather*}
%
\mathbf{h} = h_+ \mathbf{e}_+ + h_\times \mathbf{e}_\times \sts \mathbf{e}_+ \equiv \left[ \begin{array}{ccc} 1 & 0 & 0 \\ 0 & -1 & 0 \\ 0 & 0 & 0 \end{array} \right] , \ \mathbf{e}_\times \equiv \left[ \begin{array}{ccc} 0 & 1 & 0 \\ 1 & 0 & 0 \\ 0 & 0 & 0 \end{array} \right]
%
\end{gather*}
%
Satisfies the conditions on $\mathbf{h}$ and can be rotated about any spatial axis to describe a gravitational wave along any different direction.
%
Rather after using an algebraic solver to compute Christoffel symbols:
%
\begin{align*}
%
G^{1}_{\ 1} - G^{2}_{\ 2} &= \ddot{h}_+ + 3H\dot{h}_+ - \frac{h_{+,zz}}{a^2} \\
%
G^{1}_{\ 2} &= \frac{1}{2}\left(\ddot{h}_\times + 3H\dot{h}_\times - \frac{h_{\times,zz}}{a^2}\right) \\
%
\text{Generalizing } \partial_z &\rightarrow \hat{k} \cdot \svec{\nabla} \text{ and Fourier transforming}:\\
%
\mathcal{F}\left(G^{1}_{\ 1} - G^{2}_{\ 2}\right) &= \ddot{\tilde{h}}_+ + 3H\dot{\tilde{h}}_+ + \frac{k^2}{a^2}\tilde{h}_+\\
%
\mathcal{F}\left(G^{1}_{\ 2}\right) &= \frac{1}{2}\left(\ddot{\tilde{h}}_\times + 3H\dot{\tilde{h}}_\times + \frac{k^2}{a^2}\tilde{h}_\times\right) \\ 
%
&\sts h_\alpha = \int \frac{d^3k}{(2 \pi)^3} e^{i\svec{k}\cdot\svec{x}} \tilde{h}_\alpha \\
%
&\sts \alpha \in \{ +, \times \}
%
\end{align*}
%
Assuming scalar inflation does not source the tensor anisotropic stress $\pi^T_{ij}$ in \myref{appn:cosm:svt2} then we get that $\delta T^1_{\ 2} = 0$ and $T^1_{\ 1} - T^2_{\ 2} = 0$. So from there we have (dropping the tildes as we'll stay in Fourier space):
%
\begin{gather*}
%
\ddot{h}_\alpha + 3H\dot{h}_\alpha + \frac{k^2}{a^2}h_\alpha = 0 \\ 
%
\text{In conformal time we get} \\
%
h_\alpha\pp + 2\mathcal{H}h_\alpha\p + k^2 h_\alpha = 0
%
\end{gather*}
%
Which is the wave equation with a Hubble drag term confirming that these are gravitational waves. 
%
\end{namebox}



\section{$E$ and $B$ decomposition}



\begin{namebox}{Polarization to $E$- and $B$-modes}{appn:cosm:decomp}

Without circular polarzation, we can put $Q$ and $U$ into a tensor:
%
\begin{gather*}
%
\mat{P}\left( \hat{n} = (\theta, \phi) \right) = \frac{1}{\sqrt{2}}\left( \begin{array}{cc}%
%
Q\left(\hat{n}\right) & U\left(\hat{n}\right)\sin(\theta) \\  
%
U\left(\hat{n}\right)\sin(\theta) & -Q\left(\hat{n}\right)\sin^2(\theta) \end{array} 
%
\right)
%
\end{gather*}
%
Following \cite{Kamionkowski:2016}:
Similar to the temperature decomposition in \myref{cosm:ylms}, this tensor can be written as the sum:
%
\begin{gather*}
%
P_{ab}\left(\hat{n}\right) = T_0 \sum^\infty_{\ell = 2}\sum^\ell_{m = -\ell} \left( a^E_{\ell m} Y^{E}_{\left( \ell m \right) a b}\left( \hat{n}\right) +  a^B_{\ell m} Y^{B}_{\left( \ell m \right) a b}\left( \hat{n}\right) \right) \\
%
\sts \\
%
Y^{E}_{\left( \ell m \right) a b}\left( \hat{n}\right) = N_\ell \left( Y_{\left(\ell m\right):ab} - \frac{1}{2}g_{ab} Y_{\left(\ell m\right):c}^{\ \ \ \ c}\right) \\
%
Y^{B}_{\left( \ell m \right) a b}\left( \hat{n}\right) = \frac{N_\ell}{2} \left( Y_{\left(\ell m\right):ac}\epsilon^c_{\ b} + Y_{\left(\ell m\right):bc}\epsilon^c_{\ a}\right) \\ 
%
N_{\ell} \equiv \sqrt{\frac{2(\ell - 2)!}{(\ell + 2)!}}
%
\end{gather*}
%
To compute the coefficients:
%
\begin{gather*}
%
a^{E}_{\ell m} = \frac{N_{\ell}}{T_0} \int d\hat{n} Y^*_{\ \ell m}\left(\hat{n}\right)P_{ab}^{\ \ :ab}\left(\hat{n}\right) \\
%
a^{B}_{\ell m} = \frac{N_{\ell}}{T_0} \int d\hat{n} Y^*_{\ \ell m}\left(\hat{n}\right)P_{ab}^{\ \ :ac}\left(\hat{n}\right) \epsilon_c^{\ b} \\
%
\end{gather*}
%
\end{namebox}
