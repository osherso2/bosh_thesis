\chapter*{Appendix C\\Extra Results}
\addcontentsline{toc}{chapter}{Appendix C: Extra Results}
\def \chapterlabel {C}
\label{appn:extras}
\setcounter{eqnum}{1}\setcounter{fignum}{1}



\section{Leg analysis}\label{appn:extras:leg_analysis}

\subsection{Building the catalogue}
Where does a particle have to land on a plane in order for it to contribute to the statistics of a particular leg's spectra? To make the required math easier, we can put the source at the origin (this is how the sims are configured anyway) and pick axes such that the leg is in the first quadrant and parallel to $\hat{x}$.
%
Further, because the sources are isotropic, any event deposited at $\vec{r} = (r_x, r_y)$ can be forced into the first quadrant: $\vec{r} = (|r_x|, |r_y|)$.
%
Now working with angles is easier.
%
Consider the diagram and variables in \myref{appn:extras:shape_geom}:



\begin{boxfig}{Calculating area weights}{./shape_geom.png}{appn:extras:shape_geom}
  The source is at the origin. A leg of length (of length $L$) has its midpoint distance is $d$ away. An event lands at $\vec{r}$. If $b_1 < r < b_2 \sts r \equiv |\vec{r}|$, then this event can be rotated onto the leg.
\end{boxfig}



\begin{align*}
   \vec{d} &\equiv (d_x, d_y) \\
   b_1 &= \sqrt{\left(d_x - L/2\right)^2 + d_y^2} \\
   b_2 &= \sqrt{\left(d_x + L/2\right)^2 + d_y^2}
\end{align*}
An event that falls at $\vec{r}$ is catalogued if for $r \equiv |\vec{r}|$, $b_1 < r < b_2$. We want to measure, for all events that fall in the green region bounded by $(b_1, b_2)$, the distance from the event to the island.
%
Again arguing the isotropic nature of the source, any event with the right $r$ could have equally probably occured on the leg such that $r_y = d_y$.
%
Doing this rotation gives;
\begin{align*}
  r_y\p &= d_y \\
  r_x\p &= \sqrt{r^2 - d_y^2}
\end{align*}
Now that the island and the event are aligned along the $x$-axis the distance is;
\begin{gather*}
  f = \begin{array}{ll}
  d_x + L/2 - \sqrt{r^2 - d_y^2} & \text{for a leg to the left of the island} \\
  d_x - L/2 - \sqrt{r^2 - d_y^2} & \text{for a leg to the right of the island} \end{array} \\
  f \equiv 0 \text{ if event is on the island.}
\end{gather*}
Where I made $f$ negative for the case where the leg is to the right of the island.
%
Saving a catalogue of ($f$, energy) for each event is not sufficient to reconstruct the spectrum as a function of distance from island.
%
I must also save from which source each event comes from so that we can later divide from the rate the area enhancement from using hits from the entire annuli.

So here is the catalog building process:
\begin{itemize}
    \item From the raw data, iterate through every combination of event, detector and source to create catalogue of $(det, source, f, E, leg)$ (the leg is determined simply from which layer stack the event was recorded),
    \item From each valid detector, source, combination, find the $weight$ of that event.
    \item Next we need a transfer function $F(det, source, f, E)$ to create the more useful catalogue of $(det, E_{transfered}, weight)$.
\end{itemize}

\subsection{Weight}

Each `hit' might come from a different source and thus require a different rate-correction factor, $weight$, which should convert the event into a rate in $Hz$.

Each run simulates 2000000 events.
%
For our $0.9 \mu Ci$ ($ 3.33 \cdot 10^7 Bq$ ) sources, that means each run is simulating about $.06$s.
%
I run a thousand sims or $\Delta t = 60.06$ s.

For each (source, detector) pair, there is an acceptance area, $\pi \left( b_2^2 - b_1^2 \right)^2$ from the definitions above.
%
But since each leg only has an area, $A_{leg}^{i}$ for the $i^{th}$ leg, the count of events is increased by a factor of $g_{a} \equiv \pi \left( b_2^2 - b_1^2 \right) / A_{leg}^i$.
%
Note that because there are four identical rail legs, the $A_{leg}^{RL}$ must include this factor of four increase.
%
In addition, because I run four sims, one for each layer stack, the overall enhancement factor for all legs is increased by another factor of four.

So overall, a single event on a leg must be treated as contributing $weight \ [Hz]$ to the overall rate where $weight \equiv (4 \cdot \Delta t \cdot g_a)^{-1}$.

\section{Thermal model}

Let's model a leg of length $L$ with a temperature profile $T(x)$.
%
In keeping with the definition of $f$ above we need $x=0, T_1$ to be the island side.
%
If the conductance per length of a leg follows a power law;
$$g = g_* \left(\frac{T}{T_*}\right)^{\beta} \left[ \frac{W}{mK} \right],$$
then the temperature profile of the leg is such that the power flowing through that leg, $P = g\left(T(x)\right) dT/dx$ is constant across the leg. We can construct a version of Bernoulli's differential equation;
\begin{align*}
    \frac{dT}{dx} &= \frac{P}{g_*}\left(\frac{T_*}{T}\right)^{\beta} \\
    \Rightarrow T(x) &= \left((1 + \beta)\frac{PT_*^\beta}{g_*}x + C\right)^{\frac{1}{1 + \beta}}
\end{align*}
Solving for the constants, knowing that $P(x) = P$;
\begin{align*}
    P &= \frac{1}{L} \int_0^L dx g\left(T(x)\right) \frac{dT}{dx} \\
    &= \ \frac{1}{L} \int_{T_1}^{T_2} dT g\left(T\right) \sts T_1 \equiv T(0), \ T_2 \equiv T(L)\\
    &= \frac{g_*\left( T_2^{\beta + 1} - T_1^{\beta + 1}\right)}{LT_*^\beta\left( \beta + 1 \right)}  < 0 \text{ (heat flowing off island)}\\
    T(x) &= \left(\left( T_2^{\beta + 1} - T_1^{\beta + 1}\right)\frac{x}{L} + T_1^{1 + \beta}\right)^{\frac{1}{1 + \beta}}
\end{align*}
From $P = g\left(T(x)\right) dT/dx$, I can find that conductance always has a simple general form;
\begin{align*}
    G(T(x_1), T(x_2)) &= \frac{1}{\int_{x_1}^{x_2} \frac{d\tilde{x}}{g(\tilde{x})}} = \frac{P}{T(x_2) - T(x_1)}
\end{align*}
The hard part is finding $P$ and $T(x)$ which I already did above.
%
Writing this out isn't pretty but we have everything.

For small depositions we expect the fraction of energy transfered is;
\begin{align*}
    F(f, leg) &= \frac{R(T(f), T_2)}{R(T_1, T_2)} = \frac{G(T_1, T_2)}{G(T(f), T_2)} \\
    \Rightarrow F(f, leg) &= \frac{P / (T_1 - T_2)}{P / (T(f) - T_2)} \\
    F(f) &= \frac{T(f) - T_2}{T_1 - T_2}
\end{align*}
Which ends up relying on only $\beta$, $L$, the end-points and $f$.



\subsection*{Preliminary results}



The results are more events at intermediate energies, regardless of the interpolation method, which helps fill in the expected gap between the high energy bump and the lower energy bulk, see \myref{appn:extras:new_hist}.



\begin{boxfig}{New histogram}{leg_prop.png}{appn:extras:new_hist}
%
The interpolation method shifts the smeared bump, but both methods similarly add events between the bulk and the bump.
%

\end{boxfig}


Because of the small area of the legs, the additional events in the bulk low-energy region are not sufficient to model the $100 - 10,000 eV$ bins of the measured spectra.
%
Creating a transfer function from events occuring in the wafer seems like the logical next place to look.
%
Because we measured an excess of low energy events, the energy deposited into the wafer by the particles must disperse and only a fraction of the deposited energy reaches the TES.
%
This does not conflict with our posterior belief that detectors are not sensitive to events beyond $\mathcal{O}(1 mm)$ past the island.



\section{\cmbsIV ($L,C$) choices}

These are the $L$ and $C$ values I thought worked best best on my analysis described in chapter \ref{cmbs4}.



\begin{namebox}{Recommended $(L, C)$s}{appn:extras:LandCs}
    \begin{center}
        $R_n = 8 m\Omega$

        \begin{tabular}{|c||cc|cc|}
            \hline
            $GHz$ & $C \ [pJ/K]$ & $L \ [\mu H]$ & $\tau_- \ [ms]$ & detector aliasing penalty \\ \hline
            30  & 0.75 & 1.75 & 2.54 & 0.1\% \\
            40  & 0.75 & 1.00 & 1.12 & 0.2\% \\ \hline
            85  & 1.50 & 1.00 & 1.12 & 0.1\% \\
            145 & 1.50 & 0.70 & 0.66 & 0.3\% \\ \hline
            95  & 1.55 & 0.95 & 1.06 & 0.2\% \\
            155 & 1.55 & 0.70 & 0.76 & 0.2\% \\ \hline
            220 & 2.95 & 0.65 & 0.79 & 0.2\% \\
            270 & 2.95 & 0.50 & 0.55 & 0.3\% \\ \hline
        \end{tabular}

        \ \\
        $R_n = 12 m\Omega$

        \begin{tabular}{|c||cc|cc|}
            \hline
            $GHz$ & $C \ [pJ/K]$ & $L \ [\mu H]$ & $\tau_- \ [ms]$ & detector aliasing penalty \\ \hline
            30  & 0.75 & 2.00 & 2.58 & 0.1\% \\
            40  & 0.75 & 1.35 & 1.09 & 0.2\% \\ \hline
            85  & 1.50 & 1.25 & 1.12 & 0.2\% \\
            145 & 1.50 & 1.00 & 0.63 & 0.3\% \\ \hline
            95  & 1.55 & 1.25 & 1.05 & 0.3\% \\
            155 & 1.55 & 1.00 & 0.73 & 0.3\% \\ \hline
            220 & 2.95 & 0.95 & 0.74 & 0.2\% \\
            270 & 2.95 & 0.75 & 0.51 & 0.3\% \\ \hline
        \end{tabular}

        \ \\
        $R_n = 16 m\Omega$

        \begin{tabular}{|c||cc|cc|}
            \hline
            $GHz$ & $C \ [pJ/K]$ & $L \ [\mu H]$ & $\tau_- \ [ms]$ & detector aliasing penalty \\ \hline
            30  & 0.75 & 2.50 & 2.54 & 0.1\% \\
            40  & 0.75 & 1.85 & 1.04 & 0.2\% \\ \hline
            85  & 1.50 & 1.75 & 1.07 & 0.2\% \\
            145 & 1.50 & 1.25 & 0.63 & 0.4\% \\ \hline
            95  & 1.55 & 1.75 & 0.99 & 0.2\% \\
            155 & 1.55 & 1.35 & 0.70 & 0.3\% \\ \hline
            220 & 2.95 & 1.30 & 0.71 & 0.2\% \\
            270 & 2.95 & 1.00 & 0.50 & 0.3\% \\ \hline
        \end{tabular}
    \end{center}
\end{namebox}
