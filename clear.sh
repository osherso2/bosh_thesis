rm main.aux
rm main.bbl
rm main.blg
rm main.log
rm main.out
rm main.toc
rm texput.log 
rm chapters/*/fill.aux
rm chapters/*/main_chapter.aux
rm chapters/*/main_chapter.log
rm chapters/*/main_chapter.out
rm chapters/*/texput.log
rm appendices/*/fill.aux
rm appendices/*/main_chapter.aux
rm appendices/*/main_chapter.log
rm appendices/*/main_chapter.out
rm appendices/*/texput.log

